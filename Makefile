all:
	make -C src/ozctapi
	make -C src/ozscrlx

install:
	make -C src/ozctapi $@
	make -C src/ozscrlx $@
	make -C etc $@

clean:
	make -C src/ozctapi $@
	make -C src/ozscrlx $@
