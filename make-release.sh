#!/bin/sh

if [ -z $1 ]; then
	echo $0 release-name
	exit 1
fi

RELEASE_TAG=`echo $1 | sed 's/^\([0-9]\)/v\1/'`
VERSION=`echo $1 | sed 's/v//'`

git tag $RELEASE_TAG
git archive --format=tar --prefix=o2scr/ $RELEASE_TAG | bzip2 > o2scr-$VERSION.tar.bz2
gpg -sb o2scr-$VERSION.tar.bz2
