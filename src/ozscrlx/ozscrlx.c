/*
 *   OZSCR PCMCIA SmartCardBus Reader Driver Kernel module for 2.6 kernel
 *   Copyright (C) 2005-2006 O2Micro Inc.

 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.

 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.

 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 *  O2Micro Inc., hereby disclaims all copyright interest in the
 *  library ozscrlx.ko written by Jeffrey Dai

 *    Module:       ozscrlx.c
 *    Author:       O2Micro Inc.,
 *    Modified in 2006 by Eric Piel <eric.piel@tremplin-utc.net>
 */

/*
 *
 * IRQ : One non-sharable IRQ
 * I/O : Length 0x20, 16bits. => instead of 8 bits
 * Mem : Length 0x20, 16bits, 200ns => instead of 8 bits
 * 
 */
#define OZSCR_IRQACK	0x04

/*
 * Commands to the OZSCR are TLV (Tag, Length, Value) coded and placed into
 * the I/O buffer starting at byte 2. If the length of a command exceeds 28
 * bytes (32 - 2 - 2) the command is sent in pieces of maximum 28 bytes with
 * a handshake performed after each chunk. The tag of such a command is
 * OR'ed with 0x04 (except for the last chunk). The maximum length of data
 * that can be transmitted to or read from the OZSCR is 256 bytes. When
 * data is received from the OZSCR the data is tagged with the same tag
 * as the command sent to get the data with bit 2 set.
 */
#define OZSCR_CLSE	0x10	/* Close session tag */
#define OZSCR_SLCT	0x50	/* Select card tag */

#define OZSCR_BUFSZ	64
#define ATR_SIZE        0x40	/* TS + 32 + SW + PROLOGUE + EPILOGUE... */

/*MANUAL PORT Register Map*/
#define VCC 0x80
#define VPP 0x40
#define RST 0x20
#define CLK 0x10
#define IO  0x08

#include <linux/fs.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/ptrace.h>
#include <linux/slab.h>
#include <linux/string.h>
#include <linux/timer.h>
#include <linux/interrupt.h>
#include <linux/delay.h>
#include <linux/version.h>
#include <linux/module.h>

#include <asm/uaccess.h>
#include <asm/io.h>
#if LINUX_VERSION_CODE < KERNEL_VERSION(3,4,0)
#include <asm/system.h>
#endif

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,36)
#include <pcmcia/cs_types.h>
#endif
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,37)
#include <pcmcia/cs.h>
#endif
#include <pcmcia/cistpl.h>
#include <pcmcia/ds.h>

#include "ozscrlx.h"

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,26)
#define device                          class_device
#define device_create(a, b, c, d, e...) class_device_create(a, b, c, NULL, ##e)
#define device_destroy(a, b)            class_device_destroy(a, b)
#elif LINUX_VERSION_CODE < KERNEL_VERSION(2,6,27)
#define device_create(a, b, c, d, e...) device_create_drvdata(a, b, c, d, ##e)
#endif

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,28)
#undef pcmcia_parse_tuple
#define pcmcia_parse_tuple(a, b) pccard_parse_tuple(a, b)
#endif

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,33)
#define pcmcia_request_window(a, b, c) pcmcia_request_window(&a, b, c)
#define pcmcia_map_mem_page(a, b,c )   pcmcia_map_mem_page(b, c)
#endif

#undef MODULE_NAME
#define MODULE_NAME	"OZSCRLX "

/* Never heard of more than one on the same box, so 4 should be more than enough */
#define MAX_O2SCR_DEV 4

u16 Fi[] = { 372, 372, 558, 744, 1116, 1488, 1860, 0xFFFF, 0xFFFF,
	512, 768, 1024, 1536, 2048, 0xFFFF, 0xFFFF
};
u16 Di[] = { 0xFFFF, 1, 2, 4, 8, 16, 32, 0xFFFF, 12, 20, 0xFFFF,
	0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF
};

/* Correct Table */
static u16 crctab[256] = {
0x0000, 0x1189, 0x2312, 0x329b, 0x4624, 0x57ad, 0x6536, 0x74bf,
0x8c48, 0x9dc1, 0xaf5a, 0xbed3, 0xca6c, 0xdbe5, 0xe97e, 0xf8f7,
0x1081, 0x0108, 0x3393, 0x221a, 0x56a5, 0x472c, 0x75b7, 0x643e,
0x9cc9, 0x8d40, 0xbfdb, 0xae52, 0xdaed, 0xcb64, 0xf9ff, 0xe876,
0x2102, 0x308b, 0x0210, 0x1399, 0x6726, 0x76af, 0x4434, 0x55bd,
0xad4a, 0xbcc3, 0x8e58, 0x9fd1, 0xeb6e, 0xfae7, 0xc87c, 0xd9f5,
0x3183, 0x200a, 0x1291, 0x0318, 0x77a7, 0x662e, 0x54b5, 0x453c,
0xbdcb, 0xac42, 0x9ed9, 0x8f50, 0xfbef, 0xea66, 0xd8fd, 0xc974,
0x4204, 0x538d, 0x6116, 0x709f, 0x0420, 0x15a9, 0x2732, 0x36bb,
0xce4c, 0xdfc5, 0xed5e, 0xfcd7, 0x8868, 0x99e1, 0xab7a, 0xbaf3,
0x5285, 0x430c, 0x7197, 0x601e, 0x14a1, 0x0528, 0x37b3, 0x263a,
0xdecd, 0xcf44, 0xfddf, 0xec56, 0x98e9, 0x8960, 0xbbfb, 0xaa72,
0x6306, 0x728f, 0x4014, 0x519d, 0x2522, 0x34ab, 0x0630, 0x17b9,
0xef4e, 0xfec7, 0xcc5c, 0xddd5, 0xa96a, 0xb8e3, 0x8a78, 0x9bf1,
0x7387, 0x620e, 0x5095, 0x411c, 0x35a3, 0x242a, 0x16b1, 0x0738,
0xffcf, 0xee46, 0xdcdd, 0xcd54, 0xb9eb, 0xa862, 0x9af9, 0x8b70,
0x8408, 0x9581, 0xa71a, 0xb693, 0xc22c, 0xd3a5, 0xe13e, 0xf0b7,
0x0840, 0x19c9, 0x2b52, 0x3adb, 0x4e64, 0x5fed, 0x6d76, 0x7cff,
0x9489, 0x8500, 0xb79b, 0xa612, 0xd2ad, 0xc324, 0xf1bf, 0xe036,
0x18c1, 0x0948, 0x3bd3, 0x2a5a, 0x5ee5, 0x4f6c, 0x7df7, 0x6c7e,
0xa50a, 0xb483, 0x8618, 0x9791, 0xe32e, 0xf2a7, 0xc03c, 0xd1b5,
0x2942, 0x38cb, 0x0a50, 0x1bd9, 0x6f66, 0x7eef, 0x4c74, 0x5dfd,
0xb58b, 0xa402, 0x9699, 0x8710, 0xf3af, 0xe226, 0xd0bd, 0xc134,
0x39c3, 0x284a, 0x1ad1, 0x0b58, 0x7fe7, 0x6e6e, 0x5cf5, 0x4d7c,
0xc60c, 0xd785, 0xe51e, 0xf497, 0x8028, 0x91a1, 0xa33a, 0xb2b3,
0x4a44, 0x5bcd, 0x6956, 0x78df, 0x0c60, 0x1de9, 0x2f72, 0x3efb,
0xd68d, 0xc704, 0xf59f, 0xe416, 0x90a9, 0x8120, 0xb3bb, 0xa232,
0x5ac5, 0x4b4c, 0x79d7, 0x685e, 0x1ce1, 0x0d68, 0x3ff3, 0x2e7a,
0xe70e, 0xf687, 0xc41c, 0xd595, 0xa12a, 0xb0a3, 0x8238, 0x93b1,
0x6b46, 0x7acf, 0x4854, 0x59dd, 0x2d62, 0x3ceb, 0x0e70, 0x1ff9,
0xf78f, 0xe606, 0xd49d, 0xc514, 0xb1ab, 0xa022, 0x92b9, 0x8330,
0x7bc7, 0x6a4e, 0x58d5, 0x495c, 0x3de3, 0x2c6a, 0x1ef1, 0x0f78
};

static unsigned int CmdResetInterface(READER_EXTENSION *pRdrExt);
static unsigned int CmdResetReader(READER_EXTENSION *pRdrExt,
				   BOOLEAN WarmReset, u8 *pATR,
				   unsigned long *pATRLength);

static unsigned int CmdDeactivate(READER_EXTENSION *pRdrExt);
static unsigned int CBTransmit(READER_EXTENSION *pRdrExt);

static char version[] = "O2Micro SmartCardBus Reader (for kernel >= 2.6.17)";

/*
 * The probe() and detach() entry points are used to create and destroy
 * "instances" of the driver, where each instance represents everything
 * needed to manage one actual PCMCIA card.
 */
static int ozscr_probe(struct pcmcia_device *p_dev);
static void ozscr_detach(struct pcmcia_device *p_dev);
static int ozscr_resume(struct pcmcia_device *p_dev);
static void ozscr_release(struct pcmcia_device *p_dev);

/*
 * A array of "instances" of the OZSCR device.  Each actual
 * PCMCIA card corresponds to one device instance, and is described
 * by one pcmcia_device structure (defined in ds.h).
 */
static struct pcmcia_device *p_dev_list[MAX_O2SCR_DEV] = {NULL, NULL, NULL, NULL};

/* card-reader class */
static struct class *o2scr_class;
static int o2scr_major;

/*
 * Private data for OZSCR reader. Need to provide a dev_node_t
 * structure for the device.
 * FIX: Possibly needs to be extended to support PRG encryption and/or
 *	downloading of additional drivers to the OZSCR.
 */
typedef struct ozscr_dev_t {
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,35)
	dev_node_t node;	/* associated device node           */
#endif
	int minor;		/* minor id of associated char dev  */
	caddr_t am_base;	/* Base of mapped attribute memory  */
	caddr_t cm_base;	/* We need to handle common memory */
	unsigned long io_base;	/* Base of I/O port range           */
	int irq;		/* irq assigned to this card reader */
	READER_EXTENSION *o2scr_reader;	/* the internals of the card reader */
} ozscr_drv_t;

static struct pcmcia_device_id ozscrlx_ids[] = {
	PCMCIA_DEVICE_PROD_ID123("O2Micro", "SmartCardBus Reader", "V1.0",
				 0x97299583, 0xB8501BA9, 0xE611E659),
	PCMCIA_DEVICE_NULL
};

MODULE_DEVICE_TABLE(pcmcia, ozscrlx_ids);

static struct pcmcia_driver ozscrlx_driver = {
	.owner = THIS_MODULE,
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,37)
	.name = "ozscrlx_cs",
#else /* < 2.6.37 */
	.drv = {
		.name = "ozscrlx_cs",
		},
#endif /* < 2.6.37 */
	.id_table = ozscrlx_ids,
	.probe = ozscr_probe,
	.remove = ozscr_detach,
	// nothing special for suspending (at least it suspend)
//      .suspend        = ozscr_suspend,
//      XXX after resume, the reader doesn't not work until an eject/insert with pccardctl
	.resume = ozscr_resume,
};

/* Parameter that can be set with "insmod" */
static int mem_speed = 0;	/* in ns */
module_param(mem_speed, int, 0444);

static u8 ct;			/*Contact current value */
static PSCR_REGISTERS *sync_IOBase;
static u8 *sync_membase;

#ifdef PCMCIA_DEBUG
// XXX should we use dev_dbg()?
#define dprintk(x, args...) do {					\
		printk(KERN_DEBUG MODULE_NAME "%s: " x, __FUNCTION__, ##args);	\
	} while (0)
#else
#define dprintk(x, args...) do { } while (0)
#endif

#if 0
/*
 * interrupt handler
 */
static irqreturn_t ozscr_interrupt(int irq, void *dev_id, struct pt_regs *regs)
{
	struct ozscr_dev_t *dev = (struct ozscr_dev_t *)dev_id;
	unsigned char ack;

	dprintk("interrupt\n");
	if (dev == NULL)
		return IRQ_NONE;

	/* Acknowledge interrupt to reader. */
	ack = inb(dev->io_base);
	ack &= ~OZSCR_IRQACK;
	outb(ack, dev->io_base);

	return IRQ_HANDLED;
}
#endif

/*
 * open channel on OZSCR
 */
static int ozscr_open(struct inode *inode, struct file *file)
{
	int minor = iminor(inode);
	struct pcmcia_device *p_dev = p_dev_list[minor];

	dprintk("called\n");

	if (p_dev == NULL || !pcmcia_dev_present(p_dev))
		return -ENODEV;

	return 0;		//XXX should this be set as non-seekable ?
}

/*
 * close channel on OZSCR
 */
static int ozscr_close(struct inode *inode, struct file *file)
{
	int minor = iminor(inode);
	struct pcmcia_device *p_dev = p_dev_list[minor];

	dprintk("called\n");

	if (p_dev == NULL || !pcmcia_dev_present(p_dev))
		return -ENODEV;

	return 0;
}

/*
 *	All of the card reader operations are currently performed through
 *	ioctl's. This is because the semantics of read and write are not
 *	easily mapped onto reading/writing a card through the ISO interface.
 *
 *	The ioctl's that do not have the possibility to return values other
 *	than a status return EIO for all possible problems. The ioctl's that
 *	copy information back to the user fill the reader status bit and
 *	succeed, even if the reader had an error.
 */
static long ozscr_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
	struct pcmcia_device *p_dev;
	struct ozscr_dev_t *dev;
	READER_EXTENSION *pRdrExt;
	int ret;
	unsigned int size = _IOC_SIZE(cmd);	/* size for data transfers  */
	unsigned long ATRLength;
	struct ozscr_apdu apdu;
	u8 ATRBuffer[ATR_SIZE];	/* TLVList[16]; */

	p_dev = p_dev_list[iminor(file->f_dentry->d_inode)];
	if (p_dev == NULL || !pcmcia_dev_present(p_dev))
		return -ENODEV;

	dev = (struct ozscr_dev_t *)p_dev->priv;
	pRdrExt = dev->o2scr_reader;
	if ((cmd & IOC_IN) && (!access_ok(VERIFY_READ, (char *)arg, size)))
		return -EFAULT;
	if ((cmd & IOC_OUT) && (!access_ok(VERIFY_WRITE, (char *)arg, size)))
		return -EFAULT;
	// Just to check if some lines are necessary
	if ((void *)pRdrExt->membase != dev->am_base)
		printk(KERN_WARNING "membase != am_base\n");

	// TODO (eric) Needs locking (cf Oliver Neukum)
	switch (cmd) {
	case OZSCR_RESET:	/* Reset CT */
		dprintk("OZSCR_RESET\n");
		pRdrExt->membase = dev->am_base;	//XXX necessary?
		ret = CmdResetInterface(pRdrExt);
		break;
	case OZSCR_OPEN:	/* Request ICC */
		dprintk("OZSCR_OPEN\n");
		ATRLength = ATR_SIZE;
		pRdrExt->IOBase = (PSCR_REGISTERS *) dev->io_base;	//XXX necessary?
		pRdrExt->membase = dev->am_base;	//XXX necessary?

		pRdrExt->m_SCard.AvailableProtocol = 0;
		pRdrExt->m_SCard.RqstProtocol = 0;
		pRdrExt->m_SCard.WireProtocol = 0;
		dprintk("membase:%p\n", pRdrExt->membase);
		dprintk("ioport:0x%03x\n", (unsigned)((long)pRdrExt->IOBase));

		ret = CmdResetReader(pRdrExt, FALSE, ATRBuffer, &ATRLength);
		apdu.LengthOut = ATRLength;

#ifdef PCMCIA_DEBUG
		printk(KERN_DEBUG "Open finished, ATR buffer = ");
		for (ATRLength = 0; ATRLength < apdu.LengthOut; ATRLength++)
			printk(" [%02X] ", ATRBuffer[ATRLength]);
		printk("\n");
#endif

		memcpy(apdu.DataOut, ATRBuffer, ATRLength);
		apdu.Status = pRdrExt->m_SCard.WireProtocol;
		if (copy_to_user((struct ozscr_apdu *)arg, &apdu,
				 sizeof(struct ozscr_apdu)))
			return -EFAULT;
		break;
	case OZSCR_CLOSE:	/* Eject ICC */
		dprintk("OZSCR_CLOSE\n");
		pRdrExt->membase = dev->am_base;

		ret = CmdDeactivate(pRdrExt);
		break;
	case OZSCR_SELECT:
		dprintk("OZSCR_SELECT test clk\n");

		/* set protocol */
		if (copy_from_user(&apdu, (struct ozscr_apdu *)arg,
				   sizeof(struct ozscr_apdu)))
			return -EFAULT;
		ATRLength = ATR_SIZE;
		pRdrExt->IOBase = (PSCR_REGISTERS *) dev->io_base;
		pRdrExt->membase = dev->am_base;
		pRdrExt->m_SCard.RqstProtocol = apdu.DataIn[6];
		dprintk("membase:%p\n", pRdrExt->membase);
		dprintk("ioport:0x%03x\n", (unsigned)((long)pRdrExt->IOBase));
		ret = CmdResetReader(pRdrExt, FALSE, ATRBuffer, &ATRLength);
		apdu.LengthOut = ATRLength;
		memcpy(apdu.DataOut, ATRBuffer, ATRLength);
		if (copy_to_user((struct ozscr_apdu *)arg, &apdu,
				 sizeof(struct ozscr_apdu)))
			return -EFAULT;
		break;
	case OZSCR_STATUS:	/* Get Status */
		dprintk("OZSCR_STATUS\n");
		pRdrExt->membase = dev->am_base;
		ret = readw(pRdrExt->membase + STATUS_EXCH);
		break;
	case OZSCR_CMD:
		dprintk("OZSCR_CMD protocol=%d\n", pRdrExt->m_SCard.Protocol);

		if (copy_from_user(&apdu, (struct ozscr_apdu *)arg,
				   sizeof(struct ozscr_apdu)))
			return -EFAULT;
		pRdrExt->IOBase = (PSCR_REGISTERS *) dev->io_base;
		pRdrExt->membase = dev->am_base;
		dprintk("In data transfer: apdu.length = %hd\n",
			(u16) apdu.LengthIn);
#ifdef PCMCIA_DEBUG
		printk(KERN_DEBUG "APDU datain = ");
		for (ATRLength = 0; ATRLength < apdu.LengthIn; ATRLength++)
			printk("[%02X] ", apdu.DataIn[ATRLength]);
		printk("\n");
		printk(KERN_DEBUG "%s\n", apdu.DataIn + 5);
#endif
		pRdrExt->SmartcardRequest.Buffer = apdu.DataIn;
		pRdrExt->SmartcardRequest.BufferLength = apdu.LengthIn;
		pRdrExt->SmartcardReply.Buffer = apdu.DataOut;

		pRdrExt->SmartcardReply.BufferLength = apdu.LengthOut;

		ret = CBTransmit(pRdrExt);
		apdu.LengthOut = pRdrExt->SmartcardReply.BufferLength;
#ifdef PCMCIA_DEBUG
		printk(KERN_DEBUG "Dump FIFO (a.L=%2hX) ",
		       (u16) apdu.LengthOut);
		printk(" (r.L=%2hX) ",
		       (u16) pRdrExt->SmartcardReply.BufferLength);
		printk("\n");
#endif

		if (copy_to_user((struct ozscr_apdu *)arg, &apdu,
				 sizeof(struct ozscr_apdu)))
			return -EFAULT;
		break;
	default:
		ret = -EINVAL;
		break;
	}

	return ret;
}

#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,37)

static int ozscr_ioprobe(struct pcmcia_device *p_dev, void *priv_data)
{
	return pcmcia_request_io(p_dev);
}

#elif LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,36)

static int ozscr_ioprobe(struct pcmcia_device *p_dev,
			 cistpl_cftable_entry_t *cfg,
			 cistpl_cftable_entry_t *dflt,
			 unsigned int vcc, void *priv_data)
{
	p_dev->conf.ConfigIndex = cfg->index;
	p_dev->resource[0]->start = cfg->io.win[0].base;

	return pcmcia_request_io(p_dev);
}

#elif LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,33)

static int ozscr_ioprobe(struct pcmcia_device *p_dev,
			 cistpl_cftable_entry_t *cfg,
			 cistpl_cftable_entry_t *dflt,
			 unsigned int vcc, void *priv_data)
{
	p_dev->conf.ConfigIndex = cfg->index;
	p_dev->io.BasePort1 = cfg->io.win[0].base;

	return pcmcia_request_io(p_dev, &p_dev->io);
}

#else /* < 2.6.33 */

/*
 * Card service configuration
 * Helper function to get a tuple, get it's data and parse it.
 */
static int get_tuple(struct pcmcia_device *handle, tuple_t *tuple, cisparse_t *parse)
{
	int err;
	dprintk("func begin\n");

	err = pcmcia_get_tuple_data(handle, tuple);
	if (err)
		return err;

	err = pcmcia_parse_tuple(tuple, parse);
	dprintk("func end\n");

	return err;
}

#endif /* < 2.6.33 */

/*
 *  ozscr_config() is run at the end of an attach event
 *  to configure the PCMCIA socket, and to make the
 *  device available to the system.
 */
static int ozscr_config(struct pcmcia_device *p_dev)
{
	struct ozscr_dev_t *dev = p_dev->priv;
	int err, minor;
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,37)
	win_req_t req;
#endif
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,36)
	memreq_t mem;
#endif /* < 2.6.36 */

#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,33)
	err = pcmcia_loop_config(p_dev, ozscr_ioprobe, NULL);
	if (err)
		goto error_return;

#else /* < 2.6.33 */
	tuple_t tuple;
	cisparse_t parse;
	cistpl_cftable_entry_t *cf = &parse.cftable_entry;
	unsigned char buf[OZSCR_BUFSZ];

	/* This reads the card's CONFIG tuple to find its configuration registers. */
	tuple.Attributes = 0;
	tuple.TupleData = buf;
	tuple.TupleDataMax = sizeof(buf);
	tuple.TupleOffset = 0;
	tuple.DesiredTuple = CISTPL_CONFIG;

	dprintk("get tuple\n");

	err = pcmcia_get_first_tuple(p_dev, &tuple);
	if (err) {
		dev_dbg(&p_dev->dev, "ParseTuple: %d\n", err);
		goto error_return;
	}
	err = get_tuple(p_dev, &tuple, &parse);
	if (err) {
		dev_dbg(&p_dev->dev, "ParseTuple: %d\n", err);
		goto error_return;
	}

	dprintk("GetFirstTuple Complete\n");

	p_dev->conf.ConfigBase = parse.config.base;
	p_dev->conf.Present = parse.config.rmask[0];

	/* Configure card Find I/O port for the card. */
	tuple.Attributes = 0;
	tuple.TupleData = buf;
	tuple.TupleDataMax = sizeof(buf);
	tuple.TupleOffset = 0;
	tuple.DesiredTuple = CISTPL_CFTABLE_ENTRY;

	dprintk("config card (Find IO port)\n");

	err = pcmcia_get_first_tuple(p_dev, &tuple);
	if (err) {
		dev_dbg(&p_dev->dev, "ParseTuple: %d\n", err);
		goto error_return;
	}
	err = get_tuple(p_dev, &tuple, &parse);
	while (!err) {
		if (cf->io.nwin > 0) {
			p_dev->conf.ConfigIndex = cf->index;
			p_dev->io.BasePort1 = cf->io.win[0].base;
			err = pcmcia_request_io(p_dev, &p_dev->io);
			if (!err)
				break;
		}
		err = pcmcia_get_next_tuple(p_dev, &tuple);
		if (err) {
			dev_dbg(&p_dev->dev, "ParseTuple: %d\n", err);
			goto error_return;
		}
		err = get_tuple(p_dev, &tuple, &parse);
	}

	if (err) {
		dev_dbg(&p_dev->dev, "RequestIO: %d\n", err);
		goto error_return;
	}
#endif /* < 2.6.33 */

#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,36)
	dprintk("IO Port 0x%03x\n", p_dev->resource[0]->start);
#endif /* < 2.6.36 */

#if 0
	dprintk("Begin to request IRQ\n");
	/* Configure card Now allocate an interrupt line. (IRQ) */
	if ((ret = pcmcia_request_irq(p_dev, &p_dev->irq)) != CS_SUCCESS) {
		dprintk("Request IRQ fail!\n");
		cs_error(p_dev, RequestIRQ, ret);
		goto error_return;
	}
	dprintk("Request IRQ successful (IRQ: %d)\n", p_dev->irq.AssignedIRQ);
#endif

	/* Set up the I/O window and the interrupt mapping. */
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,37)
	err = pcmcia_enable_device(p_dev);
#else /* < 2.6.37 */
	err = pcmcia_request_configuration(p_dev, &p_dev->conf);
#endif /* < 2.6.37 */
	if (err) {
		dev_dbg(&p_dev->dev, "RequestConfiguration: %d\n", err);
		goto error_return;
	}
	dprintk("setup I/O Win & INT map ok\n");

	/* Allocate a 2K memory window for the attribute space. This
	   contains four registers of interest. */
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,37)
	p_dev->resource[2]->flags |= WIN_DATA_WIDTH_16 | WIN_MEMORY_TYPE_CM |
			WIN_ENABLE;
	p_dev->resource[2]->start = 0;
	p_dev->resource[2]->end = 0x1000;
	err = pcmcia_request_window(p_dev, p_dev->resource[2], mem_speed);
#else /* < 2.6.37 */
	req.Attributes = WIN_DATA_WIDTH_16 | WIN_MEMORY_TYPE_CM | WIN_ENABLE;
	req.Base = 0;
	req.Size = 0x1000;	/* Request 2K memory *///XXX strange, this is 4K !
	req.AccessSpeed = mem_speed;
	p_dev->win = (window_handle_t) p_dev;
	err = pcmcia_request_window(p_dev, &req, &p_dev->win);
#endif /* < 2.6.37 */
	if (err) {
		dev_dbg(&p_dev->dev, "RequestWindow: %d\n", err);
		goto error_return;
	}
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,37)
	dev->cm_base = (caddr_t) p_dev->resource[2]->start;
	dev->am_base = ioremap(p_dev->resource[2]->start, 0x1000);
	err = pcmcia_map_mem_page(p_dev, p_dev->resource[2], 0);
#else /* < 2.6.37 */
	dev->cm_base = (caddr_t) req.Base;
	dev->am_base = ioremap(req.Base, 0x1000);	//XXX iounmap is only in detach, enough?
#if LINUX_VERSION_CODE == KERNEL_VERSION(2,6,36)
	err = pcmcia_map_mem_page(p_dev, p_dev->win, 0);
#else /* < 2.6.36  */
	mem.CardOffset = 0x0;
	mem.Page = 0;
	err = pcmcia_map_mem_page(p_dev, p_dev->win, &mem);
#endif /* < 2.6.36 */
#endif /* < 2.6.37 */
	if (err) {
		dev_dbg(&p_dev->dev, "MapMemPage: %d\n", err);
		goto error_return;
	}

	dprintk("am_base:%p\n", dev->am_base);

	/* Find an available minor for the device */
	for (minor = 0; minor < MAX_O2SCR_DEV; minor++)
		if (p_dev_list[minor] == NULL)
			break;
	if (minor >= MAX_O2SCR_DEV) {
		printk(KERN_WARNING
		       "No more minor available for the new o2scr device.\n");
		goto error_return;
	}
	p_dev_list[minor] = p_dev;

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,35)
	/* Initialize the dev_node_t structure */
	sprintf(dev->node.dev_name, "ozscrlx");
	dev->node.major = o2scr_major;
	dev->node.minor = minor;
#endif /* < 2.6.35 */
	dev->minor = minor;
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,36)
	dev->io_base = p_dev->resource[0]->start;
#else /* < 2.6.36 */
	dev->io_base = p_dev->io.BasePort1;
#endif /* < 2.6.36 */
#if 0
	dev->irq = p_dev->irq.AssignedIRQ;
#endif

	dprintk("OZSCR device loaded\n");
	return 0;

      error_return:
	/* If any step failed, release any partially configured state */
	ozscr_release(p_dev);
	return -ENODEV;
}

/*
 *  ozscr_probe() creates an "instance" of the driver, allocating
 *  local data structures for one device.  The device is registered
 *  with Card Services.
 *
 *  The pcmcia_device structure is initialized, but we don't actually
 *  configure the card at this point -- we wait until we receive a
 *  card insertion event.
 */
static int ozscr_probe(struct pcmcia_device *p_dev)
{
	struct ozscr_dev_t *dev;
	int ret = -ENOMEM;

	dprintk("function begins\n");

	/* Allocate space for private device-specific data */
	dev = kzalloc(sizeof(struct ozscr_dev_t), GFP_KERNEL);
	if (dev == NULL) {
		dprintk("allocate ozscr_dev_t fail\n");
		goto ErrHandle;
	}

	/* Allocate space for private device-specific data */
	dev->o2scr_reader = kzalloc(sizeof(READER_EXTENSION), GFP_KERNEL);
	if (dev->o2scr_reader == NULL) {
		dprintk("allocate READER_EXTENSION fail\n");
		goto ErrHandle;
	}

	p_dev->priv = dev;

#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,36)
	p_dev->resource[0]->end = 32;
	p_dev->resource[0]->flags &= ~IO_DATA_PATH_WIDTH;
	p_dev->resource[0]->flags |= IO_DATA_PATH_WIDTH_16;
	p_dev->resource[1]->end = 0;
	p_dev->io_lines = 5;
#else /* < 2.6.36 */
	/* The io structure describes IO port mapping */
	p_dev->io.NumPorts1 = 32;
	p_dev->io.Attributes1 = IO_DATA_PATH_WIDTH_16;
	p_dev->io.NumPorts2 = 0;
	p_dev->io.IOAddrLines = 5;
#endif /* < 2.6.36 */

#if 0
	/* Interrupt setup */
	p_dev->irq.Attributes = IRQ_TYPE_EXCLUSIVE | IRQ_HANDLE_PRESENT;
	p_dev->irq.IRQInfo1 = IRQ_LEVEL_ID;
	p_dev->irq.Handler = &ozscr_interrupt;
	p_dev->irq.Instance = dev;
#endif

#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,37)
	p_dev->config_flags |= /*CONF_ENABLE_IRQ |*/ CONF_AUTO_SET_IO;
	p_dev->vpp = 50;
	p_dev->config_regs = PRESENT_OPTION | PRESENT_STATUS;
#else /* < 2.6.37 */
	/* General socket nfiguration */
	p_dev->conf.Attributes = /*CONF_ENABLE_IRQ*/ 0;
	p_dev->conf.Vpp = 50;
	p_dev->conf.IntType = INT_MEMORY_AND_IO;
	p_dev->conf.Present = PRESENT_OPTION | PRESENT_STATUS;
#endif /* < 2.6.37 */

	ret = ozscr_config(p_dev);
	if (ret)
		goto ErrHandle;

	// it's just so redundant... we could merge those fields together
	dev->o2scr_reader->IOBase = (PSCR_REGISTERS *) dev->io_base;
	dev->o2scr_reader->membase = dev->am_base;

	CmdResetInterface(dev->o2scr_reader);

	/*
	 * Only one device handled for now, but let's number it in case
	 * one day we can support more.
	 */
	device_create(o2scr_class, NULL, MKDEV(o2scr_major, dev->minor),
		      NULL, "o2scr%d", dev->minor);

	dprintk("function complete\n");
	return 0;

      ErrHandle:
	/* Free the allocated memory space */
	if (dev)
		kfree(dev->o2scr_reader);
	kfree(dev);
	return ret;
}

/*
 * After a card is removed, ozscr_release() will unregister the
 * device, and release the PCMCIA configuration.
 */
static void ozscr_release(struct pcmcia_device *p_dev)
{
/*
 * If the device is still in use we may not release resources
 * right now but we must ensure that no further actions are
 * done on the device. The resources we still hold will be
 * eventually freed through ozscr_detach.
 */
	dprintk("called\n");
	pcmcia_disable_device(p_dev);

	return;
}

/*
 *  This deletes a driver "instance".  The device is de-registered
 *  with Card Services.  If it has been released, all local data
 *  structures are freed.
 */
static void ozscr_detach(struct pcmcia_device *p_dev)
{
	struct ozscr_dev_t *dev = (struct ozscr_dev_t *)p_dev->priv;
	int i;
	dprintk("called\n");

	//needed ? XXX -eric
	flush_scheduled_work();

	ozscr_release(p_dev);

	/* clean up behind us */
	if (dev) {		/* it should never be NULL but this is for safety */
		device_destroy(o2scr_class, MKDEV(o2scr_major, dev->minor));
		kfree(dev->o2scr_reader);
		iounmap(dev->am_base);
		kfree(dev);
		p_dev->priv = NULL;
	}

	/* remove from the list of devices */
	for (i = 0; i < MAX_O2SCR_DEV; i++) {
		if (p_dev == p_dev_list[i]) {
			p_dev_list[i] = NULL;
			break;
		}
	}

	return;
}

/*
 * This resume a card which was put to sleep. If nothing is done, the reader is not
 * accessible anymore. So we need to reset it, I guess, this is purely experimental...
 * as it doesn't work at all (still needs a eject/insert cycle to get it back)
 */
static int ozscr_resume(struct pcmcia_device *p_dev)
{
	//struct ozscr_dev_t *dev = (struct ozscr_dev_t *)p_dev->priv;
	dprintk("called\n");

	//CmdResetInterface(dev->o2scr_reader);
	// XXX maybe we should try to relaunch ozscr_probe?
	return 0;
}

static ssize_t ozscr_read(struct file *file,
			  char *pucRxBuffer, size_t count, loff_t *loc)
{
	dprintk("called\n");
	return 1;
}

static ssize_t ozscr_write(struct file *file,
			   const char *pucTxBuffer, size_t count, loff_t *loc)
{
	dprintk("called\n");
	return 1;
}

static struct file_operations ozscr_chr_fops = {
	.owner = THIS_MODULE,
	.read = ozscr_read,
	.unlocked_ioctl = ozscr_ioctl,
	.write = ozscr_write,
	.open = ozscr_open,
	.release = ozscr_close,
};

/* =========================================================================
   Entry/Ending point of the driver
*/
static int __init init_ozscrlx(void)
{
	printk(KERN_INFO MODULE_NAME "%s\n", version);

	o2scr_class = class_create(THIS_MODULE, "ozscrlx");
	if (!o2scr_class)
		return -1;

	/* Register new character device with kernel */
	o2scr_major = register_chrdev(0, "ozscrlx", &ozscr_chr_fops);
	if (!o2scr_major) {
		dprintk("Grab device fail#%d\n", o2scr_major);
		class_destroy(o2scr_class);
		return -1;
	}

	if (pcmcia_register_driver(&ozscrlx_driver) < 0) {
		unregister_chrdev(o2scr_major, "ozscrlx");
		class_destroy(o2scr_class);
		return -1;
	}

	dprintk("major num: %d\n", o2scr_major);

	return 0;
}

static void __exit exit_ozscrlx(void)
{
	int i;
	dprintk("unloading module\n");

	/* free all the registered devices */
	for (i = 0; i < MAX_O2SCR_DEV; i++)
		if (p_dev_list[i])
			ozscr_detach(p_dev_list[i]);

	pcmcia_unregister_driver(&ozscrlx_driver);
	unregister_chrdev(o2scr_major, "ozscrlx");
	class_destroy(o2scr_class);
	return;
}

/*
Description: Since all memory or I/O R/W is 16 bits, even we need
             read a byte.
Procedure: Read Word returns the man_low byte
*/

static inline u8 o2scr_readb(u8 *address)
{
	return (u8) readw(address);
}

static inline void o2scr_writeb(u8 *address, u8 byte)
{
	writew(byte, address);
}

static inline u8 o2scr_inb(u8 *address)
{
	unsigned short us;
	us = inw((unsigned long)address);
	return (u8) (us >> 8);
}

static inline void o2scr_outb(u8 *address, u8 byte)
{
	outw((u16) byte << 8, (unsigned long)address);
}

static inline void o2scr_writel(unsigned long *address, unsigned long ul)
{
	writew((u16) (ul >> 16), address);
	writew((u16) ul, (unsigned long *)(((u16 *) address) + 1));
}

/*
    Description: Excute command correspond to exeCmd
    Procedure:
    EXE.exeCmd = 1 

    Wait for STATUS_IT.End_EXE =1 to see if command complete
    Check first byte in STATUS_EXCH for any error
    END OF PROCEDURE
*/
static unsigned int CmdExecuteWithTimeOutAndTOC(READER_EXTENSION *pRdrExt,
						u16 exeCmd, u16 *status_exch,
						unsigned long timeout,
						BOOLEAN SetTOC)
{
	unsigned long timeoutcount;
	unsigned int ret = STATUS_IO_DEVICE_ERROR;
	u8 *membase;
	u8 uc;

	membase = pRdrExt->membase;
	writew(exeCmd, membase + EXE);

	for (timeoutcount = timeout; timeoutcount > 0; timeoutcount--) {

		/*need check STATUS_IT.End_EXE to see if command complete */
		uc = o2scr_readb(membase + STATUS_IT);
		if (uc & END_EXE) {
			ret = STATUS_SUCCESS;
			/* Clear correspond bit in DEVAL_IT */
			o2scr_writeb(membase + DEVAL_IT, ~END_EXE_CLR_B);
			break;
		}

		msleep(1);
	}

	*status_exch = readw(membase + STATUS_EXCH);
	if (SetTOC)
		writew(S_TOC_EXE, membase + EXE);
	return ret;
}

static inline unsigned int
CmdExecuteWithTimeOut(READER_EXTENSION *pRdrExt, u16 exeCmd, u16 *status_exch,
		      int timeout)
{
	return CmdExecuteWithTimeOutAndTOC(pRdrExt, exeCmd, status_exch,
					   timeout, FALSE);
}

static inline unsigned int
CmdExecute(READER_EXTENSION *pRdrExt, u16 exeCmd, u16 *status_exch)
{
	return CmdExecuteWithTimeOut(pRdrExt, exeCmd, status_exch, CMD_MAX_DELAY);
}

/* Launch the command and don't wait for it finished or not */
static inline void CmdExecuteWithNoWait(READER_EXTENSION *ReaderExtension,
					u16 exeCmd)
{
	u8 *membase;
	membase = ReaderExtension->membase;

	writew(exeCmd, membase + EXE);
}

/*
	Description: Clear FIFO
	Procedure:
	EXE.RST_FIFO_EXE = 1 
	'need check STATUS_IT.End_EXE to see if command complete
	'The STATUS_EXCH.FIFO_EMPTY = 1
	END OF PROCEDURE
*/
static unsigned int CmdClearFIFO(READER_EXTENSION *pRdrExt)
{
	unsigned int ret = STATUS_SUCCESS;
	u16 status_exch;
	u8 *membase = pRdrExt->membase;

	dprintk("function start\n");

	/*Check if FIFO already empty or not */
	if (readw(membase + FIFO_NB) == 0)
		return ret;

	/*EXE.RST_FIFO_EXE = 1 */
	ret = CmdExecute(pRdrExt, RST_FIFO_EXE, &status_exch);
	/*Check if STATUS_EXCH.FIFO_EMPTY = 1 */
	if (ret && !(status_exch & FIFO_EMPTY)) {
		printk(KERN_WARNING "IO DEVICE ERROR in CmdClearFIFO()\n");
		ret = STATUS_IO_DEVICE_ERROR;
	}

	dprintk("function complete\n");
	return ret;
}

/*
 * Read All DATA in FIFO_OUT
 */
static unsigned int ReadATR(READER_EXTENSION *pRdrExt, u8 *pATR,
			    unsigned long *pATRLength)
{
	PSCR_REGISTERS *IOBase = pRdrExt->IOBase;
	u8 *membase = pRdrExt->membase;
	unsigned long i, fifoLength;

	/*Read number of bytes in FIFO_NB */
	fifoLength = readw(membase + FIFO_NB);
	dprintk("fifoLength = %ld in ReadATR()\n", fifoLength);
	fifoLength &= 0X1FF;
	dprintk("fifoLength = %ld in ReadATR() after &\n", fifoLength);

	if (fifoLength > *pATRLength) {
		printk(KERN_WARNING MODULE_NAME "STATUS_BUFFER_TOO_SMALL\n");
		return STATUS_BUFFER_TOO_SMALL;
	}
	*pATRLength = fifoLength;
	pRdrExt->m_SCard.Atr_len = fifoLength;

	/*Read all bytes via FIFO_OUT */
	for (i = 0; i < *pATRLength; i++) {
		*(pATR + i) = o2scr_inb(&IOBase->FIFO_OUT);
		pRdrExt->m_SCard.ATR[i] = *(pATR + i);	// XXX this is the only different line with ReadFIFO
	}

#ifdef PCMCIA_DEBUG
	dprintk("ATR read =");
	for (i = 0; i < *pATRLength; i++)
		printk(" [%02X]", pRdrExt->m_SCard.ATR[i]);
	printk("\n");
#endif
	return STATUS_SUCCESS;
}

/*
 * Read All DATA in FIFO_OUT
 */
static unsigned int ReadFIFO(READER_EXTENSION *pRdrExt, u8 *pATR,
			     unsigned long *pATRLength)
{
	PSCR_REGISTERS *IOBase = pRdrExt->IOBase;
	u8 *membase = pRdrExt->membase;
	unsigned long i, fifoLength;

	/* Read number of bytes in FIFO_NB */
	fifoLength = readw(membase + FIFO_NB);
	dprintk("fifoLength = %ld in ReadFIFO()\n", fifoLength);
	fifoLength &= 0X1FF;
	dprintk("fifoLength = %ld in ReadFIFO() after &\n", fifoLength);
	/*Check if buffer big enough to hold the data */
	if (fifoLength > *pATRLength) {
		printk(KERN_WARNING MODULE_NAME "STATUS_BUFFER_TOO_SMALL\n");
		return STATUS_BUFFER_TOO_SMALL;
	}
	*pATRLength = fifoLength;

	for (i = 0; i < *pATRLength; i++)
		*(pATR + i) = o2scr_inb(&IOBase->FIFO_OUT);

#ifdef PCMCIA_DEBUG
	dprintk("ATR read =");
	for (i = 0; i < *pATRLength; i++)
		printk(" [%02X]", pATR[i]);
	printk("\n");
	printk("exit: data1 = %02x, data2 = %02x\n", pATR[0], pATR[1]);
#endif
	return STATUS_SUCCESS;
}

/* CmdResetInterface:

   Description: Reset the Reader. (CmdResetInterface)
   Procedure:
   'Before reset
   MASK_IT = 0 'Unmask all interrupt
   MODE.CRD_DET = 1 'Ref: T P.11 and G P.16. Card Insertion will make SC_DET = 0

   'Reset now
   EXE.RESET_EXE = 1 '??? Ref: G P.25.

   Wait for at least 1 microseconds
   EXE.RESET_EXE = 0

   ClearFIFO

   'FREQUENCE ??? 33Mhz or external ???
   FRQ_MODE = 4 'Our CLK f = 33Mhz => f/(4*2) = 4.125Mhz

   'Make interrupt available for Card Insertion/Extraction
   MASK_IT.SCP_MSK = 1
   MASK_IT.SCI_MSK = 1

   END OF PROCEDURE

Arguments:
   ReaderExtension context of call

Return Value:
   STATUS_SUCCESS
   STATUS_IO_DEVICE_ERROR
 */
static unsigned int CmdResetInterface(READER_EXTENSION *pRdrExt)
{
	unsigned int ret;
	u8 *membase;
	u8 uc;

	dprintk("function start\n");

	CmdClearFIFO(pRdrExt);
	membase = pRdrExt->membase;

	dprintk("MEM Base:%p\n", membase);
	o2scr_writeb(membase + MASK_IT, 0xf3);

	uc = o2scr_readb(membase + MODE);
	uc &= ~CRD_DET;
	o2scr_writeb(membase + MODE, uc);

	/*Reset now */
	writew(RESET_EXE, membase + EXE);

	/*Wait for at least 1 microseconds */
	msleep(1);

	/*EXE.RESET_EXE = 0 */
	writew(0x0000, membase + EXE);

	/*Wait for at least 1 microseconds */
	// XXX wait a minute, you said MICROseconds ? isn't msleep for MILLIseconds !?
	msleep(1);

	/*MODE.EDC = 1; Since the SMCLIB will calculate the EDC for us */
	uc = o2scr_readb(membase + MODE);
	uc |= EDC | ATR_TO;
	o2scr_writeb(membase + MODE, uc);
	o2scr_writel((unsigned long *)(membase + BWT_MSB), 64000);

	/*Clear FIFO */
	ret = CmdClearFIFO(pRdrExt);
	if (ret)
		return ret;

	o2scr_writeb(membase + FRQ_MODE, FRQ_DIV << 4);

	/*Ref. definition of O2_POWER_DELAY_REG for more information */
	writew(0XB00B, membase + O2_POWER_DELAY_REG);

	dprintk("function complete\n");
	return ret;
}

/* Valid for 0<= x <=3 */
static int atr_find_tx1(int x, u8 *buffer, u8 *tx1)
{
	int offset = 1;

	if (!(buffer[0] & (0x10 << x)))
		return -1;

	while (x--)
		if (buffer[0] & (0x10 << x))	//hweight8(buffer[0] & (u8)(0x1E << x));
			offset++;

	*tx1 = buffer[offset];
	return 0;
}

/* Works only for n >= 2 */
static int atr_find_txn(int x, int n, u8 *buffer, u8 *txn)
{
	int i = 1, j = 1;

	for (i = 1; i < n; i++) {
		if (!(buffer[j] & 0x80))
			return -1;

		j += hweight8(buffer[j] & 0xF0);
	};

	return atr_find_tx1(x, &buffer[j], txn);
}

static u8 ATRFindProtocol(u8 *buffer)
{
	u8 protocol = 0;

#ifdef PCMCIA_DEBUG
	int j;
	printk(KERN_DEBUG "Initial data in buffer = ");
	for (j = 0; j < 0x20; j++)
		printk("[%02x] ", buffer[j]);
	printk("\n");
#endif

	atr_find_tx1(3, buffer, &protocol);
	protocol &= 0x01;

	dprintk("Result protocol = %02x\n", protocol);
	return protocol;
}

static inline int ATRFindTA1(u8 *buffer, u8 *ta1)
{
	return atr_find_tx1(0, &buffer[1], ta1);
}

static inline int ATRFindTB1(u8 *buffer, u8 *tb1)
{
	return atr_find_tx1(1, &buffer[1], tb1);
}

static inline int ATRFindTC1(u8 *buffer, u8 *tc1)
{
	return atr_find_tx1(2, &buffer[1], tc1);
}

static inline int ATRFindTA2(u8 *buffer, u8 *ta2)
{
	return atr_find_txn(0, 2, buffer, ta2);
}

static inline int ATRFindTB2(u8 *buffer, u8 *tb2)
{
	return atr_find_txn(1, 2, buffer, tb2);
}

static inline int ATRFindTC2(u8 *buffer, u8 *tc2)
{
	return atr_find_txn(2, 2, buffer, tc2);
}

/* This could have been TA3, but this one is special, it's the IFSC */
static u8 ATRFindIFSC(u8 *buffer)
{
	int ret;
	u8 ifsc = 0;

	ret = atr_find_txn(0, 3, buffer, &ifsc);
	if (ret || ifsc < 5)
		return 32;	/* default IFSC */

	return ifsc;
}

static inline int ATRFindTB3(u8 *buffer, u8 *tb3)
{
	return atr_find_txn(1, 3, buffer, tb3);
}

static inline int ATRFindTC3(u8 *buffer, u8 *tc3)
{
	return atr_find_txn(2, 3, buffer, tc3);
}

static short InitATRParam(READER_EXTENSION *pRdrExt)
{
	u8 tmp;

	pRdrExt->m_SCard.Protocol = ATRFindProtocol(pRdrExt->m_SCard.ATR + 1);

	if (pRdrExt->m_SCard.RqstProtocol == 0x01)
		pRdrExt->m_SCard.Protocol = 0x00;
	else if (pRdrExt->m_SCard.RqstProtocol == 0x02)
		pRdrExt->m_SCard.Protocol = 0x01;

	pRdrExt->m_SCard.EdcType = 0;

	pRdrExt->m_SCard.AtrTA1 = 0xFFFF;
	pRdrExt->m_SCard.TA1 = 0x11;
	if (!ATRFindTA1(pRdrExt->m_SCard.ATR, &tmp))
		pRdrExt->m_SCard.AtrTA1 = tmp;

	pRdrExt->m_SCard.TB1 = 0xFFFF;
	if (!ATRFindTB1(pRdrExt->m_SCard.ATR, &tmp))
		pRdrExt->m_SCard.TB1 = tmp;

	pRdrExt->m_SCard.TC1 = 0xFFFF;
	if (!ATRFindTC1(pRdrExt->m_SCard.ATR, &tmp))
		pRdrExt->m_SCard.TC1 = tmp;

	if (!ATRFindTA2(pRdrExt->m_SCard.ATR, &tmp)) {
		pRdrExt->m_SCard.TA2 = tmp;
		pRdrExt->m_SCard.Protocol = tmp & 0x01;	/*Only supp T=0/1 protocol */
	}

	if (!ATRFindTB2(pRdrExt->m_SCard.ATR, &tmp))
		pRdrExt->m_SCard.TB2 = tmp;

	pRdrExt->m_SCard.TC2 = 0xFFFF;
	if (!ATRFindTC2(pRdrExt->m_SCard.ATR, &tmp))
		pRdrExt->m_SCard.TC2 = tmp;

	pRdrExt->T1.ifsc = ATRFindIFSC(pRdrExt->m_SCard.ATR);
	pRdrExt->T1.ns = 0;
	pRdrExt->T1.nr = 0;
	pRdrExt->T1.nad = 0;

	if (!ATRFindTB3(pRdrExt->m_SCard.ATR, &tmp))
		pRdrExt->m_SCard.TB3 = tmp;

	pRdrExt->m_SCard.TC3 = 0xFFFF;
	if (!ATRFindTC3(pRdrExt->m_SCard.ATR, &tmp)) {
		pRdrExt->m_SCard.TC3 = tmp;

		// XXX used to always run this code, even if tmp is ATRFindTC3 returned an error
		if (tmp & 0x40) {
			if (tmp & 0x01)
				pRdrExt->T1.rc = SC_T1_CHECKSUM_CRC;
			else
				pRdrExt->T1.rc = SC_T1_CHECKSUM_LRC;
		}
	}

	pRdrExt->m_SCard.PowerOn = TRUE;
	pRdrExt->m_SCard.WakeUp = TRUE;

	return 0;
}

static unsigned int AsyT1Ini(READER_EXTENSION *pRdrExt)
{
	unsigned long cwt, BwtT1;
	u16 Cgt, etu_wrk;
	u16 F, D;
	u8 tc3, ta1;
	u8 *membase = pRdrExt->membase;

	dprintk("Init Start\n");

	ta1 = (u8) (pRdrExt->m_SCard.TA1 & 0xff);
	if (pRdrExt->m_SCard.TA1 != 0xffff) {
		F = (ta1 & 0xf0) >> 4;
		D = ta1 & 0x000f;
		F = Fi[F];
		D = Di[D];

		if (F == 0xffff || D == 0xffff) {
			F = 372;
			D = 1;
		}
	} else {
		F = 372;
		D = 1;
	}

	Cgt = 0;
	if (pRdrExt->m_SCard.TC1 != 0xffff)
		Cgt = pRdrExt->m_SCard.TC1 & 0x00ff;
	writew((Cgt + 1), membase + CGT);

	etu_wrk = (((F * 10) / D) + 5) / 10;
	writew(etu_wrk, membase + ETU_WRK);

	cwt = 13;

	BwtT1 = 4;
	if (pRdrExt->m_SCard.TB3 != 0xffff) {
		u8 tb3 = (u8) (pRdrExt->m_SCard.TB3 & 0xff);
		cwt = tb3 & 0x0f;

		BwtT1 = ((pRdrExt->m_SCard.TB3 & 0xf0) >> 4);

		if (BwtT1 > 9)
			return GE_II_ATR_PARM;
	}
	pRdrExt->m_SCard.Bwi = (u16) BwtT1;
	cwt = 11 + (1 << cwt);
	BwtT1 = ((1 << BwtT1) * 960 * 372) / F;
	BwtT1 = BwtT1 * D + 11;

	tc3 = 0;
	if (pRdrExt->m_SCard.TC3 == 0xffff)
		pRdrExt->m_SCard.TC3 = 0;
	else {
		tc3 = (u8) (pRdrExt->m_SCard.TC3 & 0x00ff);
		if (pRdrExt->m_SCard.TC3 & 0x00fe)
			return GE_II_ATR_PARM;
	}

	if (tc3) {
		writew(0x81 | PROTO_EDC_TYP, membase + PROTO);
		pRdrExt->m_SCard.EdcType = 1;
	} else {
		writew(0x81, membase + PROTO);
		pRdrExt->m_SCard.EdcType = 0;
	}

	writew((u16) (cwt / 0x10000), membase + CWT_MSB);
	writew((u16) (cwt % 0x10000), membase + CWT_LSB);
	writew((u16) (BwtT1 / 0x10000), membase + BWT_MSB);
	writew((u16) (BwtT1 % 0x10000), membase + BWT_LSB);
	pRdrExt->T1.bwt = BwtT1;

	dprintk("Init Complete\n");
	return 0;
}

static unsigned int AsyT0Ini(READER_EXTENSION *pRdrExt)
{
	unsigned long cwt;
	u16 etu_wrk, Cgt = 0;
	u16 F, D;
	u8 ta1;
	u8 *membase = pRdrExt->membase;

	dprintk("Init Start\n");

	writew(PROTO_CP | PROTO_RC, membase + PROTO);

	ta1 = (u8) (pRdrExt->m_SCard.TA1 & 0xff);
	if (pRdrExt->m_SCard.TA1 != 0xffff) {
		F = (ta1 & 0xf0) >> 4;
		D = ta1 & 0x000f;
		F = Fi[F];
		D = Di[D];

		if (F == 0) {
			F = 372;
			D = 1;
		}
	} else {
		F = 372;
		D = 1;
	}

	if (pRdrExt->m_SCard.TC1 != 0xffff)
		Cgt = pRdrExt->m_SCard.TC1 & 0x00ff;
	writew((Cgt + 1), membase + CGT);

	etu_wrk = (((F * 10) / D) + 5) / 10;
	writew(etu_wrk, membase + ETU_WRK);

	if (pRdrExt->m_SCard.TC2 == 0xffff)
		cwt = 10;
	else
		cwt = pRdrExt->m_SCard.TC2;

	if (pRdrExt->m_SCard.TA1 != 0xffff)
		cwt = (((cwt * D * F * 10) / F) + 5) / 10;

	cwt *= 960;
	writew((u16) (cwt / 0x10000), membase + CWT_MSB);
	writew((u16) (cwt % 0x10000), membase + CWT_LSB);

	dprintk("Init Complete\n");
	return 0;
}

static void man_start_receiving(void)
{
	/*Receiving Mode */
	o2scr_writeb(sync_membase + MANUAL_E_R, 0xE0);

	msleep(1);
}

static void man_start_emitting(void)
{
	o2scr_writeb(sync_membase + MANUAL_E_R, 0x60);
}

static void set_manual_mode(void)
{
	u8 uc;
	uc = o2scr_readb(sync_membase + MODE);
	uc |= MANUAL;

	o2scr_writeb(sync_membase + MODE, uc);

	man_start_receiving();
}

static void man_vcc_on(void)
{
	/*VCC - Power On */
	o2scr_outb(&sync_IOBase->MANUAL_IN, 0x80);
	msleep(1);
	ct = o2scr_inb(&sync_IOBase->MANUAL_OUT);
}

#if 0
static void man_vcc_off(void)
{
	o2scr_outb(&sync_IOBase->MANUAL_IN, 0x00);
	msleep(1);
	ct = o2scr_inb(&sync_IOBase->MANUAL_OUT);
}
#endif

static void man_low(u8 v)
{
	ct &= ~v;
	o2scr_outb(&sync_IOBase->MANUAL_IN, ct);
}

static void man_high(u8 v)
{
	ct |= v;
	o2scr_outb(&sync_IOBase->MANUAL_IN, ct);
}

static BOOLEAN man_is_read_io(void)
{
	return (o2scr_inb(&sync_IOBase->MANUAL_OUT) & IO) != 0;
}

static void man_send_command(u8 control, u8 address, u8 data, u8 WireProtocol)
{
	int i;
	u32 controlDWord;

	controlDWord = ((u32) data << 16 | (u32) address << 8 | (u32) control);

	switch(WireProtocol) {
	case 2:
		man_low(RST);
		man_high(CLK|IO);
		msleep(1);
		man_start_emitting();
		msleep(1);
		man_low(IO);
		msleep(1);
		man_low(CLK);
		break;
	default:
		man_low(RST);
		msleep(1);
		man_start_emitting();
		msleep(1);
		man_high(RST);
		break;
	}

	for (i = 0; i < 24; i++) {
		if (controlDWord & (1 << i))
			man_high(IO);
		else
			man_low(IO);

		msleep(1);
		man_high(CLK);

		msleep(1);
		man_low(CLK);
	}

	switch(WireProtocol) {
	case 2:
		man_low(IO);
		msleep(1);
		man_high(CLK);
		msleep(1);
		man_high(IO);
		man_start_receiving();
		msleep(1);
		break;
	default:
		man_start_receiving();
		msleep(1);
		man_low(RST);
		break;
	}
}

static void man_readb(u16 addressWord, u8 WireProtocol, u8 *data, u32 total)
{
	int i;
	u8 control;
	u8 address = (u8) (addressWord & 0xFF);

	switch(WireProtocol) {
	case 2:
		control = 0x30;
		break;
	default:
		control = 0x70 | (u8) ((addressWord >> 8) & 0x03);
		break;
	}

	man_send_command(control, address, 0x00, WireProtocol);

	msleep(1);
	set_manual_mode();
	msleep(1);
	man_high(CLK);
	msleep(1);
	man_low(CLK);

	for (; total; total--, data++) {
		*data = 0;
		for (i = 0; i < 8; i++) {
			msleep(1);
			man_high(CLK);

			if (man_is_read_io())
				*data |= 1 << i;
			msleep(1);
			man_low(CLK);
		}
	}
	msleep(1);
	man_high(RST);
}

static unsigned int man_writeb(u16 addressWord, u8 data, u8 WireProtocol)
{
	u8 control;
	u8 address = (u8) (0xFF & addressWord);
	u8 oldData, i, duraCycle, chkCycle, half;
	BOOLEAN writeOnly = TRUE, ackData = TRUE;
	unsigned int ret = STATUS_UNSUCCESSFUL;

	switch(WireProtocol) {
	case 2:
		control = 0x38;
		duraCycle = 255;
		half = 124;
		chkCycle = 0;
		break;
	default:
		control = 0xCC | (u8) ((addressWord >> 8) & 0x03);
		duraCycle = 203;
		half = 103;
		chkCycle = 3;
		break;
	}

	man_readb(addressWord, WireProtocol, &oldData, 1);
	if (data == oldData)
		return STATUS_SUCCESS;

	if ((data == 0) || (data == 0xFF) || (oldData == 0xFF))
		duraCycle = half;
	else {
		u8 bit;
		for (i = 0; i < 8; i++) {
			bit = 0x80 >> i;
			if (!(oldData & bit) && (data & bit)) {
				writeOnly = FALSE;
				break;
			}
		}

	}

	if (writeOnly)
		duraCycle = half;
	chkCycle = duraCycle - chkCycle;

	man_send_command(control, address, data, WireProtocol);
	msleep(1);
	for (i = 0; i < duraCycle; i++) {
		msleep(1);
		man_high(CLK);
		if (i == chkCycle) {
			ackData = man_is_read_io();
			if (ackData)
				ret = STATUS_SUCCESS;
		}
		msleep(1);
		man_low(CLK);
	}

	if(chkCycle == duraCycle) {
		msleep(1);
		ackData = man_is_read_io();
		if (ackData)
			ret = STATUS_SUCCESS;
	}

	return ret;
}

/*
 *  Sub Function: Sync. Card Reset Power On
 */
static void SyncReset(u8 astr[4])
{
	int i, j;
	u8 c;

	dprintk("Function Start.\n");

	set_manual_mode();
	man_vcc_on();
	msleep(1);
	man_high(RST);

	msleep(1);
	man_high(CLK);
	msleep(1);
	man_low(CLK);

	msleep(1);
	man_low(RST);

	msleep(1);
	for (i = 0; i < 4; i++) {
		c = 0;
		for (j = 0; j < 8; j++) {
			man_high(CLK);

			if (man_is_read_io())
				c |= 1 << j;

			msleep(1);
			man_low(CLK);
			msleep(1);
		}

		astr[i] = c;

#ifdef PCMCIA_DEBUG
		printk(" %02hX ", c);
#endif
	}
#ifdef PCMCIA_DEBUG
	printk("\n");
#endif
}

/*
	Sub Function: COLD/HOT/GETATRBACK
	Description: Single subroutine for Cold/Hot Reset and Get ATR back
	Procedure:
        End Of Subfunction
*/
static unsigned int CmdResetReader(READER_EXTENSION *pRdrExt,
				   BOOLEAN WarmReset,
				   u8 *pATR, unsigned long *pATRLength)
{
	unsigned int ret = STATUS_IO_DEVICE_ERROR;
	PSCR_REGISTERS *IOBase = pRdrExt->IOBase;
	u8 *membase = pRdrExt->membase;
	u16 status_exch;
	u8 protocol;

	dprintk
	    ("original protocol before reset: pRdrExt->m_SCard.Protocol = %d\n",
	     pRdrExt->m_SCard.Protocol);
	/*Clear FIFO before reset the card */
	ret = CmdClearFIFO(pRdrExt);

	if (ret != STATUS_SUCCESS)
		return ret;

	if (WarmReset) {
		/*Warm Reset */
		dprintk("WarmReset\n");

		/*Restore some resetting default value before Warm Reset */
		o2scr_writel((unsigned long *)(membase + CWT_MSB), 0x2580);
		o2scr_writel((unsigned long *)(membase + BWT_MSB), 0xfa00);
		ret = CmdExecute(pRdrExt, RST_EXE, &status_exch);
	} else {
		/*Cold Reset */
		dprintk("ColdReset\n");
		/*Turn off the power if it is on */
		ret = CmdDeactivate(pRdrExt);
		CmdResetInterface(pRdrExt);

		/*EXE.PON_EXE = 1 'Power on and reset sequence */
		ret = CmdExecute(pRdrExt, PON_EXE, &status_exch);

		/* Check if STATUS_EXCH */
		if (status_exch & 0xF700)
			ret = STATUS_IO_DEVICE_ERROR;
		dprintk(" status_exch=%04X\n", status_exch);
	}

	if (ret != STATUS_SUCCESS) {
		dprintk("Sync. Card Reset!!!\n");
		sync_IOBase = IOBase;
		sync_membase = membase;
		pRdrExt->m_SCard.Protocol = 0x03;

		SyncReset(pATR);
		*pATRLength = 4;

		switch(pATR[0]&0xf0) {
		case 0x80:
			pRdrExt->m_SCard.WireProtocol=1;
			break;
		case 0x90:
			pRdrExt->m_SCard.WireProtocol=3;
			break;
		case 0xa0:
			pRdrExt->m_SCard.WireProtocol=2;
			break;
		default:
			pRdrExt->m_SCard.WireProtocol=0;
			break;
		}

		return STATUS_SUCCESS;
	}

	ReadATR(pRdrExt, pATR, pATRLength);

	InitATRParam(pRdrExt);

	protocol = pRdrExt->m_SCard.Protocol & 0x01;
	if (protocol) {
		dprintk("AsyT1Ini() is called\n");
		AsyT1Ini(pRdrExt);
	} else {
		dprintk("AsyT0Ini() is called\n");
		AsyT0Ini(pRdrExt);
	}

	if (ret != STATUS_SUCCESS)
		*pATRLength = 0;

	return ret;
}

/*++
CmdDeactivate:

	Description: Power off the card
	Procedure:
	'Following condition must meet before a Power off is possible
	'Otherwise it do nothing
	STATUS_EXCH.CRD_INS == 1 'Card Inserted
	STATUS_EXCH.CRD_ON == 1 'Card is powered

	EXE.POF_EXE = 1 'Power off

	END OF PROCEDURE

Arguments:
	ReaderExtension		context of call
	Device				requested device

Return Value:
	STATUS_SUCCESS
	error values from PscrRead / PscrWrite
--*/
static unsigned int CmdDeactivate(READER_EXTENSION *pRdrExt)
{
	unsigned int ret;
	u8 *membase = pRdrExt->membase;
	u16 us, status_exch;

	us = readw((u16 *) (membase + STATUS_EXCH));
	if (!(us & CRD_ON) || !(us & CRD_INS))
		return STATUS_SUCCESS;

	/*Power off */
	ret = CmdExecute(pRdrExt, POF_EXE, &status_exch);
	msleep(1);
	us = readw(membase + STATUS_EXCH);
	if (us & CRD_ON)
		ret = STATUS_UNSUCCESSFUL;

	return ret;
}

static unsigned int PscrWriteDirect(READER_EXTENSION *pRdrExt, u8 *pData,
				    unsigned long DataLen,
				    unsigned long *pNBytes)
{
	PSCR_REGISTERS *IOBase = pRdrExt->IOBase;
	u8 *membase = pRdrExt->membase;
	unsigned long Idx;
	unsigned int ret;
	u16 status_exch, us;

	dprintk("Function Start\n");

	if (pNBytes)
		*pNBytes = 0;

	/* S_TOC_EXE just in case some command is still running */
	CmdExecuteWithTimeOut(pRdrExt, S_TOC_EXE, &status_exch, CMD_MAX_DELAY);

	ret = CmdClearFIFO(pRdrExt);
	if (ret != STATUS_SUCCESS) {
		printk(KERN_WARNING MODULE_NAME "CmdClearFIFO() faid in PscrWriteDirect()\n");
		return ret;
	}
#ifdef PCMCIA_DEBUG
	printk(KERN_DEBUG "WriteFIFO :");
	for (Idx = 0; Idx < DataLen; Idx++)
		printk(" [%02X]", pData[Idx]);
	printk("\n");
#endif

	/*Write Data into FIFO */
	for (Idx = 0; Idx < DataLen; Idx++)
		o2scr_outb(&IOBase->FIFO_IN, pData[Idx]);

	us = readw(membase + STATUS_EXCH);
	if (us & FIFO_FULL)
		return STATUS_UNSUCCESSFUL;

	/* Check if Number of Bytes is correct */
	us = readw(membase + FIFO_NB);
	if (us != DataLen)
		return STATUS_UNSUCCESSFUL;

	/*Tell user the byte number of Data been write to FIFO Succesfuly */
	*pNBytes = DataLen;

	CmdExecuteWithTimeOut(pRdrExt, S_TOC_EXE, &status_exch, CMD_MAX_DELAY);

	ret = CmdExecuteWithTimeOutAndTOC(pRdrExt, EXCH_EXE, &status_exch,
					CMD_MAX_DELAY, TRUE);

	if (status_exch & (BAD_TS | BAD_PB | ERR_PAR | ERR_EXE))
		ret = STATUS_IO_DEVICE_ERROR;
	else if (status_exch & (TOB | TOR))	/* Randy temp. don't care TOC flag */
		ret = STATUS_IO_TIMEOUT;

	CmdExecuteWithTimeOut(pRdrExt, S_TOC_EXE, &status_exch, CMD_MAX_DELAY);

	dprintk("Function Complete\n");
	return ret;
}

static unsigned int PscrWriteDirectWithFIFOLevel(READER_EXTENSION *pRdrExt,
						 u8 *pData,
						 unsigned long DataLen,
						 unsigned long *pNBytes,
						 u16 fifoLev)
{
	unsigned int ret;
	unsigned long Idx;
	int timeoutcount;
	PSCR_REGISTERS *IOBase = pRdrExt->IOBase;
	u8 *membase = pRdrExt->membase;
	u16 status_exch, us;
	u8 uc;

	dprintk("Function Start\n");

	if (pNBytes)
		*pNBytes = 0;

	CmdExecuteWithTimeOut(pRdrExt, S_TOC_EXE, &status_exch, CMD_MAX_DELAY);

	ret = CmdClearFIFO(pRdrExt);
	if (ret != STATUS_SUCCESS)
		return ret;

	if (DataLen < 5) {
		DataLen = 5;
		pData[4] = 0;
	}

	/* Write Data into FIFO */
	for (Idx = 0; Idx < DataLen; Idx++)
		o2scr_outb(&IOBase->FIFO_IN, pData[Idx]);

	us = readw(membase + STATUS_EXCH);
	if (us & FIFO_FULL)
		return STATUS_UNSUCCESSFUL;

	/* Check if Number of Bytes is correct */
	us = readw(membase + FIFO_NB);
	if (us != DataLen)
		return STATUS_UNSUCCESSFUL;

	/* Tell user the byte number of Data been write to FIFO Succesfuly */
	*pNBytes = DataLen;

	/* EXE.EXCH_EXE = 1         'Launch the Exchange command */
	CmdExecuteWithNoWait(pRdrExt, EXCH_EXE);

	/* Wait for the command finish, FIFO_NB==0 */
	if (fifoLev <= DataLen) {
		for (timeoutcount = CMD_MAX_DELAY; timeoutcount > 0; timeoutcount--) {
			/* Check STATUS_IT.End_EXE to see if command complete */
			uc = o2scr_readb(membase + STATUS_IT);
			if (uc & END_EXE) {
				ret = STATUS_SUCCESS;
				/* Clear correspond bit in DEVAL_IT */
				o2scr_writeb(membase + DEVAL_IT, ~END_EXE_CLR_B);
				break;
			}

			us = readw(membase + FIFO_NB);
			if (us == 0)
				break;

			msleep(1);
		}
	}

	/* Wait command END_EXE or FIFO_NB==fifoLev */
	for (timeoutcount = CMD_MAX_DELAY; timeoutcount > 0; timeoutcount--) {
		/* Check STATUS_IT.End_EXE to see if command complete */
		uc = o2scr_readb(membase + STATUS_IT);
		if (uc & END_EXE) {
			ret = STATUS_SUCCESS;
			/* Clear correspond bit in DEVAL_IT */
			o2scr_writeb(membase + DEVAL_IT, ~END_EXE_CLR_B);
			break;
		}

		us = readw(membase + FIFO_NB);
		if (us == fifoLev)
			break;

		msleep(1);
	}

	/* Check what kind of error we got */
	status_exch = readw(membase + STATUS_EXCH);
	if (status_exch & (BAD_TS | BAD_PB | ERR_PAR | ERR_EXE))
		ret = STATUS_IO_DEVICE_ERROR;
	else if (status_exch & (TOC | TOB | TOR))	// XXX PscrWriteDirect doesn't care about TOC flag, should we?
		ret = STATUS_IO_TIMEOUT;

	/* S_TOC_EXE just in case command is termiate by FIFO_LEVEL_TRIGERED */
	CmdExecuteWithTimeOut(pRdrExt, S_TOC_EXE, &status_exch, CMD_MAX_DELAY);

	dprintk("Function Complete\n");
	return ret;
}

/*++
CBT0Transmit:
    finishes the callback RDF_TRANSMIT for the T0 protocol
Arguments:
    pRdrExt  context of call
Return Value:
    STATUS_SUCCESS
    STATUS_NO_MEDIA
    STATUS_TIMEOUT
    STATUS_INVALID_DEVICE_REQUEST
--*/
static unsigned int CBT0Transmit(READER_EXTENSION *pRdrExt)
{
	u8 *pRequest = pRdrExt->SmartcardRequest.Buffer;
	u8 *pReply = pRdrExt->SmartcardReply.Buffer;
	unsigned long IOBytes = 0;
	unsigned long RequestLength = pRdrExt->SmartcardRequest.BufferLength;
	unsigned int ret;
	u16 T0ExpctedReturnLen;

	dprintk("Function Start\n");

	/* Transmit pRdrExt->SmartcardRequest.Buffer to smart card */
	pRdrExt->SmartcardReply.BufferLength = 0;
	dprintk("CBT0 :: RequestLength = %ld\n", RequestLength);
	/* Since H/W may not STOP the command automaticaly. Check Return Byte */
	if (RequestLength >= 5)
		T0ExpctedReturnLen = pRequest[4];
	else
		T0ExpctedReturnLen = 0;

	if (T0ExpctedReturnLen == 0)
		T0ExpctedReturnLen = 256;

	T0ExpctedReturnLen += 2;	/*SW1 and SW2 */

	if (RequestLength > 5) {
		ret = PscrWriteDirect(pRdrExt, pRequest, RequestLength, &IOBytes);
		dprintk("RequestLength > 5 : code 048\n");
	} else {
		ret = PscrWriteDirectWithFIFOLevel(pRdrExt, pRequest,
						 RequestLength, &IOBytes,
						 T0ExpctedReturnLen);
		dprintk("RequestLength <= 5 : code 049\n");
	}

	if (ret == STATUS_SUCCESS) {
		IOBytes = MAX_T1_BLOCK_SIZE;

		ret = ReadFIFO(pRdrExt, pReply, &IOBytes);
		if (ret == STATUS_SUCCESS)
			pRdrExt->SmartcardReply.BufferLength = IOBytes;
		else
			dprintk("ReadFIFO() fail CBT0 : code 051\n");
	} else
		dprintk("Fail CBT0 : code 050\n");

	dprintk("Function Complete\n");
	return ret;
}

/* Returns LRC of data */
static u8 scT1Lrc(u8 *data, int datalen)
{
	u8 lrc = 0x00;
	int i;

	for (i = 0; i < datalen; i++)
		lrc ^= data[i];
	return lrc;
}

/* Calculates CRC of data */
static void scT1Crc(u8 *data, int datalen, u8 *crc)
{
	int i;
	u16 tmpcrc = 0xFFFF;

	for (i = 0; i < datalen; i++)
		tmpcrc =
		    ((tmpcrc >> 8) & 0xFF) ^ crctab[(tmpcrc ^ *data++) & 0xFF];
	crc[0] = (tmpcrc >> 8) & 0xFF;
	crc[1] = tmpcrc & 0xFF;
}

/* Checks RC. */
static BOOLEAN scT1CheckRc(READER_EXTENSION *pRdrExt, u8 *data,
			   unsigned long *datalen)
{
	u8 cmp[2];

	switch (pRdrExt->T1.rc) {
	case SC_T1_CHECKSUM_LRC:
		/* Check LEN. */
		if ((data[2] + 3 + 1) > *datalen) {
			dprintk("(data[2]+3+1) != *datalen: code 040\n");
			return FALSE;
		}

		if (data[data[2] + 3] == scT1Lrc(data, data[2] + 3))
			return TRUE;
		break;
	case SC_T1_CHECKSUM_CRC:
		/* Check LEN. */
		if ((data[2] + 3 + 2) > *datalen) {
			dprintk("data[2]+3+2) != *datalen: code 043\n");
			return FALSE;
		}
		scT1Crc(data, data[2] + 3, cmp);
		if (memcmp(data + data[2] + 3, cmp, 2) == 0)
			return TRUE;
		break;
	default:
		break;
	}

	dprintk("scT1CheckRc failed: 046\n");
	return FALSE;
}

/* Appends RC */
static unsigned int scT1AppendRc(READER_EXTENSION *pRdrExt, u8 *data,
				 unsigned long *datalen)
{
	switch (pRdrExt->T1.rc) {
	case SC_T1_CHECKSUM_LRC:
		data[*datalen] = scT1Lrc(data, *datalen);
		*datalen += 1;
		dprintk("SC_T1_CHECKSUM_LRC: code 038\n");
		return SC_EXIT_OK;
	case SC_T1_CHECKSUM_CRC:
		scT1Crc(data, *datalen, data + *datalen);
		*datalen += 2;
		dprintk("SC_T1_CHECKSUM_CRC: code 039\n");
		return SC_EXIT_OK;
	default:
		dprintk("scT1AppendRc() failed: code 037\n");
		return SC_EXIT_BAD_PARAM;
	}

	return SC_EXIT_OK;
}

/* Builds S-Block */
static unsigned int scT1SBlock(READER_EXTENSION *pRdrExt, int type, int dir,
			       int param, u8 *block, unsigned long *pLen)
{
	unsigned int ret;

	block[0] = pRdrExt->T1.nad;
	switch (type) {
	case SC_T1_S_RESYNCH:
		block[2] = 0x00;
		*pLen = 3;
		break;
	case SC_T1_S_IFS:
		block[2] = 0x01;
		block[3] = (u8) param;
		*pLen = 4;
		break;
	case SC_T1_S_ABORT:
		block[2] = 0x00;
		*pLen = 3;
		break;
	case SC_T1_S_WTX:
		block[2] = 0x00;
		block[3] = (u8) param;
		*pLen = 4;
		break;
	default:
		printk(KERN_WARNING MODULE_NAME "default in scT1SBlock()\n");
		return SC_EXIT_BAD_PARAM;
	}

	if (dir == SC_T1_S_REQUEST)
		block[1] = 0xC0 | (u8) type;
	else
		block[1] = 0xE0 | (u8) type;

	ret = scT1AppendRc(pRdrExt, block, pLen);
	if (ret)
		printk(KERN_WARNING MODULE_NAME
		       "Call scT1AppendRc() failed in scT1SBlock()\n");

	return ret;
}

/* Builds I-Block */
static unsigned int scT1IBlock(READER_EXTENSION *pRdrExt, BOOLEAN more,
			       u8 *data, unsigned long *datalen, u8 *block,
			       unsigned long *blocklen)
{
	block[0] = pRdrExt->T1.nad;
	block[1] = 0x00;

	if (pRdrExt->T1.ns)
		block[1] |= 0x40;

	if (more)
		block[1] |= 0x20;

	if (*datalen > pRdrExt->T1.ifsc)
		return SC_EXIT_BAD_PARAM;

	block[2] = (u8) *datalen;
	memcpy(block + 3, data, *datalen);

	*blocklen = (*datalen) + 3;
	return scT1AppendRc(pRdrExt, block, blocklen);
}

/* Builds R-Block */
static unsigned int scT1RBlock(READER_EXTENSION *pRdrExt, int type, u8 *block,
			       unsigned long *len)
{
	unsigned int ret;

	block[0] = pRdrExt->T1.nad;
	block[2] = 0x00;

	if ((type != SC_T1_R_OK) &&
	    (type != SC_T1_R_EDC_ERROR) && (type != SC_T1_R_OTHER_ERROR)) {
		dprintk("SC_EXIT_BAD_PARAM: code 035\n");
		return SC_EXIT_BAD_PARAM;
	}

	if (pRdrExt->T1.nr)
		block[1] = 0x90 | (u8) type;
	else
		block[1] = 0x80 | (u8) type;

	*len = 3;
	if ((ret = scT1AppendRc(pRdrExt, block, len)))
		return ret;

	return SC_EXIT_OK;
}

/* Returns N(R) or N(S) from R/I-Block. */
// XXX should return a u8
static u8 scT1GetN(u8 *block)
{
	/* R-Block */
	if ((block[1] & 0xC0) == 0x80)
		return ((block[1] >> 4) & 0x01);

	/* I-Block */
	if ((block[1] & 0x80) == 0x00)
		return ((block[1] >> 6) & 0x01);

	return 0;
}

/* Change IFSD. */
static unsigned int scT1ChangeIFSD(READER_EXTENSION *pRdrExt, u8 ifsd)
{
	unsigned long blocklen;
	unsigned long rblocklen = pRdrExt->SmartcardReply.BufferLength;
	unsigned int ret, errors = 0;
	BOOLEAN success = FALSE;
	u8 block[SC_T1_MAX_SBLKLEN];
	u8 *rblock = pRdrExt->SmartcardReply.Buffer;

	ret = scT1SBlock(pRdrExt, SC_T1_S_IFS, SC_T1_S_REQUEST, ifsd, block,
		       &blocklen);
	if (ret) {
		printk(KERN_WARNING MODULE_NAME
		       "Call scTiSBlock() failed in scT1ChangeIFSD()\n");
		return ret;
	}

	while (!success) {
		ret = PscrWriteDirect(pRdrExt, block, blocklen, &rblocklen);
		if (ret) {
			printk(KERN_WARNING MODULE_NAME
			       "Call PscrWriteDirect() failed in scT1ChangeIFSD()\n");
			return SC_EXIT_IO_ERROR;
		}

		msleep(10);	//XXX 3s, that's a long delay

		rblocklen = MAX_T1_BLOCK_SIZE;
		ret = ReadFIFO(pRdrExt, rblock, &rblocklen);
		if (ret == STATUS_SUCCESS) {
			if (/*(rblocklen == blocklen) &&*/ (rblock[1] == 0xE1) &&
			    scT1CheckRc(pRdrExt, rblock, &rblocklen)) {
				pRdrExt->T1.ifsreq = TRUE;
				pRdrExt->T1.ifsd = rblock[3];
				success = TRUE;
			} else {
				printk(KERN_WARNING MODULE_NAME
				       "rblocklen==blocklen) && (rblock[1]==0xE1) && "
				       "scT1CheckRc( pRdrExt, rblock, &rblocklen ) "
				       "failed in scT1ChangeIFSD()\n");
				printk(KERN_WARNING
				       " values:\n rblocklen = %ld "
				       ": blocklen = %ld\n rblock[1] = %x\n",
				       rblocklen, blocklen, rblock[1]);
				errors++;

			}
		} else
			printk(KERN_WARNING MODULE_NAME
			       "Call ReadFIFO() failed in scT1ChangeIFSD()\n");

		if (errors > 2) {
			pRdrExt->T1.ifsreq = TRUE;
			success = TRUE;
		}
	}

	return SC_EXIT_OK;
}

static unsigned int t1_send_rblock(READER_EXTENSION *pRdrExt, int type,
				   u8 *block, unsigned long *blocklen,
				   unsigned long *pNBytes)
{
	unsigned int ret;
	/* Create R-Block. */
	ret = scT1RBlock(pRdrExt, type, block, blocklen);
	if (ret) {
		printk(KERN_WARNING "scT1RBlock() failed\n");
		return ret;
	}

	/* Send R-Block. */
	ret = PscrWriteDirect(pRdrExt, block, *blocklen, pNBytes);
	if (ret) {
		dprintk("PscrWriteDirect() failed\n");
		return SC_EXIT_IO_ERROR;
	}
	return SC_EXIT_OK;
}

static unsigned int t1_send_sblock(READER_EXTENSION *pRdrExt, int type,
				   int dir, int param, u8 *block,
				   unsigned long *blocklen,
				   unsigned long *pNBytes)
{
	unsigned int ret;
	/* Create S-Block. */
	ret = scT1SBlock(pRdrExt, type, dir, param, block, blocklen);
	if (ret) {
		printk(KERN_WARNING "scT1SBlock() failed\n");
		return ret;
	}

	/* Send S-Block. */
	ret = PscrWriteDirect(pRdrExt, block, *blocklen, pNBytes);
	if (ret) {
		printk(KERN_WARNING "PscrWriteDirect() failed\n");
		return SC_EXIT_IO_ERROR;
	}
	return SC_EXIT_OK;
}

static unsigned int t1_send_iblock(READER_EXTENSION *pRdrExt,
				   BOOLEAN *more, unsigned long *sendptr,
				   u8 *block, unsigned long *blocklen,
				   unsigned long *rblocklen)
{
	unsigned long sendlen;
	unsigned long RequestLength = pRdrExt->SmartcardRequest.BufferLength;
	unsigned int ret;
	u8 *pRequest = pRdrExt->SmartcardRequest.Buffer;

	/* Make next I-Block. */
	sendlen =
	    min(RequestLength - *sendptr, (unsigned long)pRdrExt->T1.ifsc);

	if (sendlen == (RequestLength - *sendptr))
		*more = FALSE;
	ret = scT1IBlock(pRdrExt, *more, pRequest + *sendptr,
			 &sendlen, block, blocklen);
	if (ret) {
		printk(KERN_WARNING "scT1IBlock() failed: code 015\n");
		return ret;
	}
	*sendptr += sendlen;

	/* Send I-Block. */
	dprintk("PscrWriteDirect: 007\n");
	ret = PscrWriteDirect(pRdrExt, block, *blocklen, rblocklen);
	if (ret) {
		printk(KERN_WARNING "PscrWriteDirect() failed: code 016\n");
		return SC_EXIT_IO_ERROR;
	}
	return SC_EXIT_OK;
}

static unsigned int cb_t1_transmit_timeout(READER_EXTENSION *pRdrExt,
					   u8 *rblock, unsigned long *rblocklen,
					   u8 *block2, unsigned long *block2len,
					   int *timeout_err_cntr, int *timecntr)
{
	unsigned int ret;

	/* Timeout handling. */
	(*timeout_err_cntr)++;
	if (*timeout_err_cntr >= 2) {
		if (*timecntr >= 2) {
			dprintk("time_out_cntr >= 2: quit!");
			return SC_EXIT_IO_ERROR;
		}

		/* send S-Block: resync request. */
		dprintk("S-Block: send resync Request\n");
		ret = t1_send_sblock(pRdrExt, SC_T1_S_RESYNCH, SC_T1_S_REQUEST,
				     0, block2, block2len, rblocklen);
		if (ret)
			return ret;

		*timeout_err_cntr = 0;
		(*timecntr)++;
	} else {
		/* indicate SC_T1_R_OTHER_ERROR */
		printk(KERN_WARNING MODULE_NAME
		       "scT1RBlock::SC_T1_R_OTHER_ERROR in ReadFIFO error\n");
		ret = t1_send_rblock(pRdrExt, SC_T1_R_OTHER_ERROR, block2,
				   block2len, rblocklen);
		if (ret)
			return ret;
	}
	return SC_EXIT_OK;

}

static unsigned int cb_t1_transmit_rc_error(READER_EXTENSION *pRdrExt,
					    u8 *rblock, unsigned long *rblocklen,
					    u8 *block2, unsigned long *block2len,
					    int *parity_err_cntr, int *errcntr)
{
	unsigned int ret;
	if (pRdrExt->T1.rc == SC_T1_CHECKSUM_LRC) {
		if ((rblock[2] + 3 + 1) != *rblocklen)
			return t1_send_rblock(pRdrExt, SC_T1_R_OTHER_ERROR,
					      block2, block2len, rblocklen);
	} else if (pRdrExt->T1.rc == SC_T1_CHECKSUM_CRC) {
		if ((rblock[2] + 3 + 2) != *rblocklen)
			return t1_send_rblock(pRdrExt, SC_T1_R_OTHER_ERROR,
					      block2, block2len, rblocklen);
	}

	(*parity_err_cntr)++;
	if (*parity_err_cntr >= 3) {
		if (*errcntr >= 1) {
			printk(KERN_WARNING
			       "CBT1Transmit() :: errcntr >= 1. Quit !\n");
			return SC_EXIT_IO_ERROR;
		}

		/* send S-Block: resync request. */
		printk(KERN_WARNING MODULE_NAME
		       "S-Block: send resync Request: parity error\n");
		ret = t1_send_sblock(pRdrExt, SC_T1_S_RESYNCH, SC_T1_S_REQUEST, 0,
				   block2, block2len, rblocklen);
		if (ret)
			return ret;
		*parity_err_cntr = 0;
		(*errcntr)++;
	} else {
		printk(KERN_WARNING "pRdrExt->T1.rc = 0x%x\n", pRdrExt->T1.rc);
		printk(KERN_WARNING MODULE_NAME
			"scT1RBlock::SC_T1_R_EDC_ERROR in Wrong length or RC error\n ");
		ret = t1_send_rblock(pRdrExt, SC_T1_R_EDC_ERROR, block2,
				   block2len, rblocklen);
		if (ret)
			return ret;
	}

	return SC_EXIT_OK;
}

static unsigned int cb_t1_transmit_rblock(READER_EXTENSION *pRdrExt,
					  BOOLEAN lastiicc, BOOLEAN *more,
					  unsigned long *sendptr, u8 *block,
					  unsigned long *blocklen, u8 *rblock,
					  unsigned long *rblocklen, u8 *block2,
					  unsigned long *block2len,
					  int *other_err_cntr, int *rerrcntr,
					  int *quit_cntr)
{
	unsigned int ret;
	dprintk("/* R-Block */\n");
	if (lastiicc) {
		(*quit_cntr)++;
		if (*quit_cntr >= 3)
			return SC_EXIT_IO_ERROR;
		/* Card is sending I-Blocks, so send R-Block. */
		dprintk("scT1RBlock::SC_T1_R_OK in R-Block\n");

		ret = t1_send_rblock(pRdrExt, SC_T1_R_OK, block2, block2len,
				   rblocklen);
		if (ret)
			return ret;

	} else {
		if (scT1GetN(rblock) == pRdrExt->T1.ns) {
		      resend_RBlock:
			(*other_err_cntr)++;
			if (*other_err_cntr >= 3) {
				/* other_err_cntr >= 3, the third time. quit and return SC_EXIT_IO_ERROR */
				if (*rerrcntr >= 2) {
					printk(KERN_WARNING MODULE_NAME
					       "CBT1Transmit() :: rerrcntr >= 2. Quit !\n");
					return SC_EXIT_IO_ERROR;
				}

				/* send S-Block: resync request. */
				dprintk("S-Block: send resync Request\n");
				ret = t1_send_sblock(pRdrExt, SC_T1_S_RESYNCH,
						   SC_T1_S_REQUEST, 0, block2,
						   block2len, rblocklen);
				if (ret)
					return ret;
				*other_err_cntr = 0;
				(*rerrcntr)++;
			} else {
				/* N(R) is old N(S), so resend I-Block. */
				dprintk("PscrWriteDirect: 006\n");
				ret = PscrWriteDirect(pRdrExt, block, *blocklen,
						    rblocklen);
				if (ret) {
					printk(KERN_WARNING MODULE_NAME
					       "PscrWriteDirect() failed: code 012\n");
					return SC_EXIT_IO_ERROR;
				}
			}
		} else {
			/* N(R) is next N(S), so make next I-Block and send it. */
			/* Check if data available. */
			if (!(*more)) {
				printk(KERN_WARNING MODULE_NAME
				       "SC_EXIT_PROTOCOL_ERROR: code 013\n");
				(*quit_cntr)++;
				if (*quit_cntr > 3)
					return SC_EXIT_PROTOCOL_ERROR;
				else
					goto resend_RBlock;
			}
			/* Change N(S) to new value. */
			pRdrExt->T1.ns ^= 1;

			ret = t1_send_iblock(pRdrExt, more, sendptr,
					     block, blocklen, rblocklen);
			if (ret)
				return ret;
		}
	}
	return SC_EXIT_OK;
}

static unsigned int cb_t1_transmit_iblock(READER_EXTENSION *pRdrExt,
					  u8 *rblock, unsigned long *rblocklen,
					  u8 *block2, unsigned long *block2len,
					  int *ib_other_err_cntr, int *ierrcntr,
					  u8 *rsp, unsigned long *rsplen)
{
	unsigned int ret;
	dprintk("/* I-Block */\n");

	if (scT1GetN(rblock) != pRdrExt->T1.nr) {
		(*ib_other_err_cntr)++;
		if (*ib_other_err_cntr >= 3) {
			/* ib_other_err_cntr >= 3, the third time. quit and return SC_EXIT_IO_ERROR */
			if (*ierrcntr >= 2) {
				printk(KERN_WARNING MODULE_NAME
				       "CBT1Transmit() :: ierrcntr >= 2. Quit !\n");
				return SC_EXIT_IO_ERROR;
			}

			/* send S-Block: resync request. */
			dprintk
			    ("S-Block: send resync Request: i-block\n");
			ret = t1_send_sblock(pRdrExt, SC_T1_S_RESYNCH,
					   SC_T1_S_REQUEST, 0, block2,
					   block2len, rblocklen);
			if (ret)
				return ret;

			*ib_other_err_cntr = 0;
			(*ierrcntr)++;
		} else {
			/* Card is sending wrong I-Block, so send R-Block. */
			printk(KERN_WARNING MODULE_NAME
			       "scT1RBlock::SC_T1_R_OTHER_ERROR in I-Block\n");
			ret = t1_send_rblock(pRdrExt, SC_T1_R_OTHER_ERROR, block2,
					   block2len, rblocklen);
			if (ret)
				return ret;
		}
		return SC_EXIT_RETRY;
	}

	/* Copy data. */
	if (rblock[2] > (SC_GENERAL_SHORT_DATA_SIZE + 2 - *rsplen)) {
		dprintk
		    ("rblock[2]>(SC_GENERAL_SHORT_DATA_SIZE+2-rsplen): code 019\n");
		return SC_EXIT_PROTOCOL_ERROR;
	}
	memcpy(&rsp[*rsplen], &rblock[3], rblock[2]);
	*rsplen += rblock[2];
	/* Change N(R) to new value. */
	pRdrExt->T1.nr ^= 1;

	if (rblock[1] & 0x20) {
		/* More data available. */
		/* Send R-Block. */
		dprintk("scT1RBlock::SC_T1_R_OK in copy data\n");
		ret = t1_send_rblock(pRdrExt, SC_T1_R_OK, block2, block2len,
				   rblocklen);
		if (ret)
			return ret;
	} else {
		u8 *pReply = pRdrExt->SmartcardReply.Buffer;
		/* Last block. */
		if (*rsplen < 2) {
			printk(KERN_WARNING "rsplen<2: code 022\n");
			return SC_EXIT_BAD_SW;
		}

		if ((pRdrExt->T1.cse == SC_APDU_CASE_2_SHORT) ||
		    (pRdrExt->T1.cse == SC_APDU_CASE_4_SHORT)) {
			unsigned long cpylen;
			/* Copy response and SW. */
			cpylen = min(*rsplen - 2, (unsigned long)SC_GENERAL_SHORT_DATA_SIZE);	// XXX in reallity it's always *rsplen-2 because it can't be > 256
			memcpy(pReply, rsp, cpylen);
			memcpy(pReply + cpylen, &rsp[*rsplen - 2], 2);
			pRdrExt->SmartcardReply.BufferLength = cpylen + 2;
		} else {
			/* Copy only SW. */
			memcpy(pReply, &rsp[*rsplen - 2], 2);
			pRdrExt->SmartcardReply.BufferLength = 2;
		}
		dprintk("SC_EXIT_OK: code 023(Jordan) return correctly. ");
		return SC_EXIT_OK;
	}
	return SC_EXIT_RETRY;
}

static unsigned int cb_t1_transmit_sblock_ifs(READER_EXTENSION *pRdrExt,
					      u8 *rblock, unsigned long *rblocklen,
					      u8 *block2, unsigned long *block2len)
{
	unsigned int ret;
	dprintk("S-Block IFS Request\n");
	ret = scT1SBlock(pRdrExt, SC_T1_S_IFS, SC_T1_S_RESPONSE, rblock[3],
		       block2, block2len);
	if (ret) {
		printk(KERN_WARNING "scT1SBlock() failed: code 024\n");
		return ret;
	}

	dprintk("PscrWriteDirect: 012\n");
	memset(block2, 0, 5);
	*block2len = 4;
	block2[0] = rblock[0];
	block2[1] = 0xE1;
	block2[2] = rblock[2];
	block2[3] = rblock[3];
	scT1AppendRc(pRdrExt, block2, block2len);

	ret = PscrWriteDirect(pRdrExt, block2, *block2len, rblocklen);

	if (ret) {
		printk(KERN_WARNING MODULE_NAME "PscrWriteDirect() failed\n");
		return SC_EXIT_IO_ERROR;
	}
	pRdrExt->T1.ifsc = rblock[3];
	msleep(10);		//XXX 3s, quite long

	return SC_EXIT_OK;
}

static unsigned int cb_t1_transmit_sblock_abort(READER_EXTENSION *pRdrExt,
						u8 *rblock, unsigned long *rblocklen,
						u8 *block2, unsigned long *block2len)
{
	unsigned int ret;
	dprintk("S-Block ABORT Request\n");
	ret = scT1SBlock(pRdrExt, SC_T1_S_ABORT, SC_T1_S_RESPONSE, 0x00, block2,
		       block2len);
	if (ret) {
		printk(KERN_WARNING MODULE_NAME "scT1SBlock() failed\n");
		return ret;
	}

	dprintk("PscrWriteDirect: 011\n");
	ret = PscrWriteDirect(pRdrExt, block2, *block2len, rblocklen);
	if (ret != *block2len)	//XXX different from t1_send_sblock, is it normal?
	{
		printk(KERN_WARNING MODULE_NAME "PscrWriteDirect() failed\n");
		return SC_EXIT_IO_ERROR;
	}
	/* Wait BWT. */
	msleep(10);		//XXX 3s, quite long
	ret = ReadFIFO(pRdrExt, rblock, rblocklen);
	if (ret) {
		printk(KERN_WARNING MODULE_NAME "ReadFIFO() failed\n");
		return SC_EXIT_IO_ERROR;
	}
	printk(KERN_WARNING MODULE_NAME "SC_EXIT_UNKNOWN_ERROR\n");
	return SC_EXIT_UNKNOWN_ERROR;
}

static unsigned int cb_t1_transmit_sblock_wtx(READER_EXTENSION *pRdrExt,
					      u8 *rblock, unsigned long *rblocklen,
					      u8 *block2, unsigned long *block2len)
{
	unsigned int ret;
	/* define a new variable for WTX */
	u8 ucRqstWTX = rblock[3];
	unsigned long ulWTX = (pRdrExt->T1.bwt) * ucRqstWTX;

	dprintk("/* S-Block WTX Request */\n");
	dprintk("rblock[3] = %x\n", rblock[3]);
	dprintk("pRdrExt->T1.bwt = 0x%lx\n", pRdrExt->T1.bwt);
	dprintk("ulWTX = 0x%lx :: ucRqstWTX = %x\n", ulWTX, ucRqstWTX);
	writew((u16) (ulWTX / 0x10000), (pRdrExt->membase) + BWT_MSB);
	writew((u16) (ulWTX % 0x10000), (pRdrExt->membase) + BWT_LSB);
	dprintk("PscrWriteDirect: 012\n");
	memset(block2, 0, 5);

	*block2len = 5;
	block2[0] = rblock[0];
	block2[1] = rblock[1] + 0x20;
	block2[2] = rblock[2];
	block2[3] = rblock[3];
	block2[4] = rblock[4] + 0x20;
	ret = PscrWriteDirect(pRdrExt, block2, *block2len, rblocklen);
	if (ret != SC_EXIT_OK) {
		printk(KERN_WARNING MODULE_NAME "PscrWriteDirect() failed\n");
		return SC_EXIT_IO_ERROR;
	}

	return SC_EXIT_OK;
}

static unsigned int cb_t1_transmit_sblock_resync(READER_EXTENSION *pRdrExt,
						 BOOLEAN *more,
						 unsigned long *sendptr,
						 u8 *block, unsigned long *blocklen,
						 unsigned long *rblocklen)
{
	/* resend I-Block */
	pRdrExt->T1.ns = 0;
	pRdrExt->T1.nr = 0;
	sendptr = 0;
	return t1_send_iblock(pRdrExt, more, sendptr, block, blocklen,
			      rblocklen);
}

static unsigned int CBT1Transmit(READER_EXTENSION *pRdrExt)
{
#ifdef PCMCIA_DEBUG
	int i;
#endif
	unsigned int ret;
	unsigned long sendptr = 0;	/* Points to begining of unsent data. */

	u8 block[SC_T1_MAX_BLKLEN];
	unsigned long blocklen = 0;
	u8 block2[SC_T1_MAX_BLKLEN];
	unsigned long block2len;
	u8 *rblock = pRdrExt->SmartcardReply.Buffer;
	unsigned long rblocklen;
	u8 rsp[SC_GENERAL_SHORT_DATA_SIZE + 3];
	unsigned long rsplen = 0;

	BOOLEAN more = TRUE;	/* More data to send. */
	BOOLEAN lastiicc = FALSE;	/* It's ICCs turn to send I-Blocks. */

	/* two counters per kind of error */
	int timeout_err_cntr = 0;
	int timecntr = 0;

	int parity_err_cntr = 0;
	int errcntr = 0;

	int other_err_cntr = 0;
	int rerrcntr = 0;

	int ib_other_err_cntr = 0;
	int ierrcntr = 0;
	int quit_cntr = 0;

	dprintk("Function Start\n");
	dprintk("pRdrExt = %lx\n", sizeof(READER_EXTENSION));

	pRdrExt->SmartcardReply.BufferLength = 0;

#ifdef PCMCIA_DEBUG
	printk(KERN_DEBUG " @@@ Request Data from ifdtest: Length = %ld\n",
	       pRdrExt->SmartcardRequest.BufferLength);
	for (i = 0; i < pRdrExt->SmartcardRequest.BufferLength; i++)
		printk("[%02x]", pRdrExt->SmartcardRequest.Buffer[i]);
	printk("\n");
#endif
	pRdrExt->T1.ifsreq = FALSE;

	// XXX is this supposed to change pRdrExt->T1.ifsreq ?!
	writew((u16) (pRdrExt->T1.bwt / 0x10000), (pRdrExt->membase) + BWT_MSB);
	writew((u16) (pRdrExt->T1.bwt % 0x10000), (pRdrExt->membase) + BWT_LSB);

	/* Change IFSD if not already changed. */
	if (!pRdrExt->T1.ifsreq) {
		ret = scT1ChangeIFSD(pRdrExt, 0xFE);
		if (ret)
			return ret;
	}
	dprintk("IFSD changed\n");

	ret = t1_send_iblock(pRdrExt, &more, &sendptr, block, &blocklen,
			   &rblocklen);
	if (ret)
		return ret;
	dprintk("Finish IFSD Write IBlock\n");

	while (TRUE) {
		rblocklen = 270;
		ret = ReadFIFO(pRdrExt, rblock, &rblocklen);

		if ((ret != SC_EXIT_OK) || (rblocklen == 0)) {
			ret = cb_t1_transmit_timeout(pRdrExt, rblock, &rblocklen,
						   block2, &block2len,
						   &timeout_err_cntr, &timecntr);
			if (ret)
				return ret;
			continue;
		}

		/* Wrong length or RC error. */
		if (!scT1CheckRc(pRdrExt, rblock, &rblocklen)) {
			ret = cb_t1_transmit_rc_error(pRdrExt, rblock, &rblocklen,
						    block2, &block2len,
						    &parity_err_cntr, &errcntr);
			if (ret)
				return ret;
			continue;
		}

		/* R-Block */
		if ((rblock[1] & 0xC0) == 0x80) {
			ret = cb_t1_transmit_rblock(pRdrExt, lastiicc, &more,
						  &sendptr, block, &blocklen,
						  rblock, &rblocklen, block2,
						  &block2len, &other_err_cntr,
						  &rerrcntr, &quit_cntr);
			if (ret)
				return ret;
			continue;
		}
		/* Reset rerrcntr, because when it is here it had not received an
		 * R-Block.
		 *///XXX not sure where it is reset?

		/* I-Block */
		if (!(rblock[1] & 0x80)) {
			if (!lastiicc)
				pRdrExt->T1.ns ^= 1;	/* Change N(S) to new value. */
			lastiicc = TRUE;
			ret = cb_t1_transmit_iblock(pRdrExt, rblock, &rblocklen,
						    block2, &block2len,
						    &ib_other_err_cntr,
						    &ierrcntr, rsp, &rsplen);
			if (ret == SC_EXIT_RETRY)
				continue;
			return ret;	/* can be an error or an OK status */
		}

		switch (rblock[1]) {
		case 0xC1:	/* S-Block IFS Request */
			ret = cb_t1_transmit_sblock_ifs(pRdrExt, rblock,
						      &rblocklen, block2,
						      &block2len);
			if (ret)
				return ret;
			break;
		case 0xC2:	/* S-Block ABORT Request */
			return cb_t1_transmit_sblock_abort(pRdrExt, rblock,
							   &rblocklen, block2,
							   &block2len);
			break;	/* it will always return an error */
		case 0xC3:	/* S-Block ABORT Request */
			ret = cb_t1_transmit_sblock_wtx(pRdrExt, rblock,
						      &rblocklen, block2,
						      &block2len);
			if (ret)
				return ret;
			break;
		case 0xE0:	/* S-Block resync response */
			ret = cb_t1_transmit_sblock_resync(pRdrExt, &more,
							 &sendptr, block,
							 &blocklen, &rblocklen);
			if (ret)
				return ret;
			break;
		default:
			/* nothing special */
			break;
		}
	}

	/* Ooops! Should never be here. It always exit from inside the while */
	return SC_EXIT_UNKNOWN_ERROR;
}

static unsigned int VerifyByte(u8 control, u8 address, u8 data, u8 WireProtocol)
{
	BOOLEAN ackData;
	unsigned int ret = STATUS_UNSUCCESSFUL;
	int i;

	man_send_command(control, address, data, WireProtocol);
	msleep(1);

	for (i = 0; i < 2; i++) {
		msleep(1);
		man_high(CLK);
		msleep(1);
		ackData = man_is_read_io();
		man_low(CLK);
		if (!ackData && (i == 1))
			ret = STATUS_SUCCESS;
	}

	return ret;
}

static unsigned int VerifyCounter(u8 control, u8 address, u8 data,
				  u8 WireProtocol)
{
	BOOLEAN ackData;
	unsigned int ret = STATUS_UNSUCCESSFUL;
	int i;

	man_send_command(control, address, data, WireProtocol);
	msleep(1);
	for (i = 0; i < 103; i++) {
		msleep(1);
		man_high(CLK);
		ackData = man_is_read_io();	// XXX is it really necessary to read it everytime?
		msleep(1);
		man_low(CLK);
		if (ackData && (i == 100))
			ret = STATUS_SUCCESS;
	}

	return ret;
}

static unsigned int VerifyData(READER_EXTENSION *pRdrExt)
{
	u8 data, trials = 0;
	u8 *pRequest = pRdrExt->SmartcardRequest.Buffer;
	u8 *pReply = pRdrExt->SmartcardReply.Buffer;
	unsigned int ret = STATUS_UNSUCCESSFUL;

	if (pRdrExt->m_SCard.WireProtocol == 2)
		goto out;

	man_readb(1021, pRdrExt->m_SCard.WireProtocol, &data, 1);
	trials = hweight8(data);
	if (trials) {
		data = (u8) (0xFF00 >> (trials - 1));	/* truncated on purpose */
		/* Use to be:
		 * data = (trials == 8) ? 0xFE :
		 (trials == 7) ? 0xFC :
		 (trials == 6) ? 0xF8 :
		 (trials == 5) ? 0xF0 :
		 (trials == 4) ? 0xE0 :
		 (trials == 3) ? 0xC0 :
		 (trials == 2) ? 0x80 : 0x00; */

		ret = VerifyCounter(0xF2, 0xFD, data,
			pRdrExt->m_SCard.WireProtocol);
		msleep(1);

		if (ret == STATUS_SUCCESS) {
			/* enter first PSC-code byte */
			ret = VerifyByte(0xCD, 0xFE, pRequest[5],
				pRdrExt->m_SCard.WireProtocol);
			msleep(1);

			if (ret == STATUS_SUCCESS) {
				/* enter second PSC-code byte */
				ret = VerifyByte(0xCD, 0xFF, pRequest[6],
					pRdrExt->m_SCard.WireProtocol);
				msleep(1);
			}
		}

		ret = VerifyCounter(0xF3, 0xFD, 0xFF,
			pRdrExt->m_SCard.WireProtocol);
	}

out:
	pRdrExt->SmartcardReply.BufferLength = 2;
	if (ret == STATUS_SUCCESS) {
		pReply[0] = 0x90;
		pReply[1] = 0x00;
	} else {
		pReply[0] = 0x63;
		trials = (trials == 0) ? 0 : trials - 1;
		pReply[1] = 0xC0 | trials;
	}

	return SC_EXIT_OK;
}

static unsigned int ChangeVerifyData(READER_EXTENSION *pRdrExt)
{
	u8 *pRequest = pRdrExt->SmartcardRequest.Buffer;
	u8 *pReply = pRdrExt->SmartcardReply.Buffer;
	unsigned int ret = STATUS_UNSUCCESSFUL;

	if (pRdrExt->m_SCard.WireProtocol == 2)
		goto out;

	/* update first PSC-code byte */
	ret = VerifyCounter(0xF3, 0xFE, pRequest[8],
		pRdrExt->m_SCard.WireProtocol);
	msleep(1);

	if (ret == STATUS_SUCCESS) {
		/* update second PSC-code byte */
		ret = VerifyCounter(0xF3, 0xFF, pRequest[9],
			pRdrExt->m_SCard.WireProtocol);

		msleep(1);
	}

out:
	pRdrExt->SmartcardReply.BufferLength = 2;
	if (ret == STATUS_SUCCESS) {
		pReply[0] = 0x90;
		pReply[1] = 0x00;
	} else {
		pReply[0] = 0x63;
		pReply[1] = 0xC0;
	}

	return SC_EXIT_OK;
}

static unsigned int UpdateBinary(READER_EXTENSION *pRdrExt)
{
	u8 *pRequest = pRdrExt->SmartcardRequest.Buffer;
	u8 *pReply = pRdrExt->SmartcardReply.Buffer;
	unsigned int ret = STATUS_UNSUCCESSFUL;
	u8 i;
	u16 address = (pRequest[2] << 8) | pRequest[3];

	pRdrExt->SmartcardReply.BufferLength = 2;

	for (i = 0; i < pRequest[4]; i++) {
		ret = man_writeb(address + i, pRequest[5 + i],
			pRdrExt->m_SCard.WireProtocol);
		if (ret != STATUS_SUCCESS)
			break;
	}

	if (ret == STATUS_SUCCESS) {
		pReply[0] = 0x90;
		pReply[1] = 0x00;
	} else {
		pReply[0] = 0x62;
		pReply[1] = 0x00;
	}

	return SC_EXIT_OK;
}

static unsigned int ReadBinary(READER_EXTENSION *pRdrExt)
{
	u8 *pRequest = pRdrExt->SmartcardRequest.Buffer;
	u8 *pReply = pRdrExt->SmartcardReply.Buffer;
	unsigned long ReplyLength = pRequest[4] + 2;
	unsigned int ret;
	u16 address = (pRequest[2] << 8) | pRequest[3];

	man_readb(address, pRdrExt->m_SCard.WireProtocol,
		pReply, pRequest[4]);

	pRdrExt->SmartcardReply.BufferLength = ReplyLength;

	ret = STATUS_SUCCESS;	// XXX oh my god, what a test!!
	if (ret == STATUS_SUCCESS) {
		pReply[ReplyLength - 2] = 0x90;
		pReply[ReplyLength - 1] = 0x00;
	} else {
		pReply[ReplyLength - 2] = 0x62;
		pReply[ReplyLength - 1] = 0x81;
	}

	return SC_EXIT_OK;
}

static unsigned int SelectFile(READER_EXTENSION *pRdrExt)
{
	u8 *pRequest = pRdrExt->SmartcardRequest.Buffer;
	u8 *pReply = pRdrExt->SmartcardReply.Buffer;
	pRdrExt->SmartcardReply.BufferLength = 2;

	if ((pRequest[5] == 0x3F) && (pRequest[6] == 0x00)) {
		pReply[0] = 0x90;
		pReply[1] = 0x00;
	} else {
		pReply[0] = 0x6A;
		pReply[1] = 0x82;
	}

	return SC_EXIT_OK;
}

static unsigned int BadCommand(READER_EXTENSION *pRdrExt)
{
	u8 *pReply = pRdrExt->SmartcardReply.Buffer;
	pRdrExt->SmartcardReply.BufferLength = 2;
	pReply[0] = 0x6E;
	pReply[1] = 0x00;

	return SC_EXIT_OK;
}

/*
CBRawTransmit:
    finishes the callback RDF_TRANSMIT for the RAW protocol

Arguments:
    pRdrExt  context of call
Return Value:

    STATUS_SUCCESS
    STATUS_NO_MEDIA
    STATUS_TIMEOUT
    STATUS_INVALID_DEVICE_REQUEST
*/
static unsigned int CBRawTransmit(READER_EXTENSION *pRdrExt)
{
	unsigned int ret;
	u8 *pRequest = pRdrExt->SmartcardRequest.Buffer;

	dprintk("Function Start\n");

	/* Transmit pRdrExt->SmartcardRequest.Buffer to smart card */
	pRdrExt->SmartcardReply.BufferLength = 0;

	/* Interindustry Commands */
	switch (pRequest[1]) {
	case 0xA4:
		ret = SelectFile(pRdrExt);
		dprintk("SelectFile\n");
		break;
	case 0xB0:
		ret = ReadBinary(pRdrExt);
		dprintk("ReadBinary\n");
		break;
	case 0xD6:
		ret = UpdateBinary(pRdrExt);
		dprintk("UpdateBinary\n");
		break;
	case 0x20:
		ret = VerifyData(pRdrExt);
		dprintk("VerifyData\n");
		break;
	case 0x24:
		ret = ChangeVerifyData(pRdrExt);
		dprintk("ChangeVerifyData\n");
		break;
	default:
		ret = BadCommand(pRdrExt);
	}

	dprintk("Function Complete\n");

	return ret;
}

/*++
CBTransmit:
    callback handler for SMCLIB RDF_TRANSMIT
Arguments:
    pRdrExt  context of call
Return Value:
    STATUS_SUCCESS

    STATUS_NO_MEDIA
    STATUS_TIMEOUT

    STATUS_INVALID_DEVICE_REQUEST
--*/
static unsigned int CBTransmit(READER_EXTENSION *pRdrExt)
{
	unsigned int ret;
	u8 protocol = pRdrExt->m_SCard.Protocol & 0x03;
	u16 apdulen;

	if ((protocol == SCARD_PROTOCOL_T0) || (protocol == SCARD_PROTOCOL_T1)) {
		apdulen = pRdrExt->SmartcardRequest.BufferLength;
		if (apdulen < 5)
			pRdrExt->T1.cse = SC_APDU_CASE_1;
		else if (apdulen == 5)
			pRdrExt->T1.cse = SC_APDU_CASE_2_SHORT;
		else if ((apdulen - 5) == pRdrExt->SmartcardRequest.Buffer[4])
			pRdrExt->T1.cse = SC_APDU_CASE_3_SHORT;
		else
			pRdrExt->T1.cse = SC_APDU_CASE_4_SHORT;
	}

	switch (protocol) {
	case SCARD_PROTOCOL_T0:
		ret = CBT0Transmit(pRdrExt);
		break;
	case SCARD_PROTOCOL_T1:
		ret = CBT1Transmit(pRdrExt);
		break;
	case SCARD_PROTOCOL_RAW:
		ret = CBRawTransmit(pRdrExt);	// XXX a lots of code is just to support this protocol, is this protocol really needed (by some user-space application)?
		break;
	default:
		ret = STATUS_INVALID_DEVICE_REQUEST;
		break;
	}

	return ret;
}

module_init(init_ozscrlx);
module_exit(exit_ozscrlx);
MODULE_LICENSE("GPL");
